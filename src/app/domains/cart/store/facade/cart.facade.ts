import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { Observable } from 'rxjs';

import { Cart } from '../../models';
import { Dish } from '../../../dishes';

import { CartState } from '../reducers';
import { selectCartState } from '../selectors';

import { AddDish, RemoveDish, ResetCart, StartCart } from '../actions';

@Injectable()
export class CartFacade {
  constructor(private store: Store<CartState>) {}

  get cart$(): Observable<Cart> {
    return this.store.pipe(select(selectCartState));
  }

  add(dish: Dish) {
    this.store.dispatch(new AddDish({ dish }));
  }

  remove(dish: Dish) {
    this.store.dispatch(new RemoveDish({ dish }));
  }

  reset() {
    this.store.dispatch(new ResetCart());
  }

  start() {
    this.store.dispatch(new StartCart());
  }
}
