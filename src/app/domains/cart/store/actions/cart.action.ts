import { Action } from '@ngrx/store';

import { Dish } from '../../../dishes';

export enum CartActionTypes {
  AddDish = '[Cart] Add Dish',
  RemoveDish = '[Cart] Remove Dish',
  ResetCart = '[Cart] Reset Cart',
  StartCart = '[Cart] Start cart'
}

export class AddDish implements Action {
  readonly type = CartActionTypes.AddDish;
  constructor(public payload: { dish: Dish }) {}
}

export class RemoveDish implements Action {
  readonly type = CartActionTypes.RemoveDish;
  constructor(public payload: { dish: Dish }) {}
}

export class ResetCart implements Action {
  readonly type = CartActionTypes.ResetCart;
}

export class StartCart implements Action {
  readonly type = CartActionTypes.StartCart;
}

export type CartActions = AddDish | RemoveDish | ResetCart | StartCart;
