import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromCart from './cart.reducer';

export interface CartState {
  cart: fromCart.State;
}

export const reducers: ActionReducerMap<CartState> = {
  cart: fromCart.reducer
};
