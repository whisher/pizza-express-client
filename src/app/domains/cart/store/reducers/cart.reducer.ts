import { CartActionTypes, CartActions } from '../actions/cart.action';
import { Dish } from '../../../dishes';

export interface State {
  dishes: Dish[];
  isVisible: boolean;
  total: number;
}

export const initialState: State = {
  dishes: [],
  isVisible: false,
  total: 0
};

export const buildTotal = (dishes): number => {
  const reducer = (acc, dish) => acc + dish.price * dish.qty;
  return dishes.reduce(reducer, 0);
};

export function reducer(state = initialState, action: CartActions): State {
  switch (action.type) {
    case CartActionTypes.StartCart: {
      return {
        ...state,
        isVisible: true
      };
    }

    case CartActionTypes.AddDish: {
      let dishes = [];
      let current = action.payload.dish;
      const currentJustExit = state.dishes.filter(dish => {
        return dish._id === current._id;
      });

      if (currentJustExit.length > 0) {
        const _dishes = state.dishes.filter(dish => {
          return dish._id !== current._id;
        });
        let last = currentJustExit.pop();
        let currentQty = last.qty;
        currentQty++;
        last = Object.assign({}, last, {
          qty: currentQty
        });
        dishes = [..._dishes, last];
      } else {
        current = Object.assign({}, current, {
          qty: 1
        });
        dishes = [...state.dishes, current];
      }
      const total = buildTotal(dishes);
      return {
        ...state,
        total,
        dishes
      };
    }

    case CartActionTypes.RemoveDish: {
      let current = action.payload.dish;
      let dishes = state.dishes.filter(dish => {
        return dish._id !== current._id;
      });
      if (current.qty > 1) {
        let currentQty = current.qty;
        currentQty--;
        current = Object.assign({}, current, {
          qty: currentQty
        });
        dishes = [...dishes, current];
      }
      const total = buildTotal(dishes);
      return {
        ...state,
        total,
        dishes
      };
    }

    case CartActionTypes.ResetCart: {
      return {
        ...initialState
      };
    }
  }

  return state;
}

export const selectCart = (state: State) => state;
