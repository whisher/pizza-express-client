import { createSelector, createFeatureSelector } from '@ngrx/store';

import { CartState } from '../reducers';
import * as fromCart from '../reducers/cart.reducer';

export const selectCartStatus = createFeatureSelector<CartState>('cart');
export const selectCartState = createSelector(
  selectCartStatus,
  (state: CartState) => {
    return state.cart;
  }
);
