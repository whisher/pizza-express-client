// Core
import { NgModule } from '@angular/core';

// Libs
import { StoreModule } from '@ngrx/store';

// Store
import { reducers } from './store';

@NgModule({
  imports: [StoreModule.forFeature('cart', reducers)]
})
export class DomainCartModule {}
