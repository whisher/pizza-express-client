import { Dish } from '../../dishes';

export interface Cart {
  dishes: Dish[];
  total: number;
}
