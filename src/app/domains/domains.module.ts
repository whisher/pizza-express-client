// Core
import {
  NgModule,
  ModuleWithProviders,
  Optional,
  SkipSelf
} from '@angular/core';

// Ngrx
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  NavigationActionTiming,
  StoreRouterConnectingModule
} from '@ngrx/router-store';

// Env
import { environment } from '../../environments/environment';

// Store
import { reducer, RouterEffects, CustomRouterStateSerializer } from './router';
import { metaReducers } from './metareducer';

// Modules
import { DomainAccountModule } from './account/account.module';
import { DomainAuthModule } from './auth/auth.module';
import { DomainCartModule } from './cart/cart.module';
import { DomainCategoriesModule } from './categories/categories.module';
import { DomainDishesModule } from './dishes/dishes.module';
import { DomainOrdersModule } from './orders/orders.module';
import { DomainUsersModule } from './users/users.module';

// Facades
import { AccountFacade } from './account/store/facade';
import { AuthFacade } from './auth/store/facade';
import { CartFacade } from './cart/store/facade';
import { CategoriesFacade } from './categories/store/facade';
import { DishesFacade } from './dishes/store/facade';
import { OrdersFacade } from './orders/store/facade';
import { UsersFacade } from './users/store/facade';

@NgModule({
  imports: [
    StoreModule.forRoot(reducer, { metaReducers }),
    EffectsModule.forRoot([RouterEffects]),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
      serializer: CustomRouterStateSerializer,
      navigationActionTiming: NavigationActionTiming.PostActivation
    }),
    StoreDevtoolsModule.instrument({
      name: 'iwdf',
      maxAge: 10,
      logOnly: environment.production
    }),
    DomainAccountModule.forRoot(),
    DomainAuthModule.forRoot(),
    DomainCartModule,
    DomainCategoriesModule.forRoot(),
    DomainDishesModule.forRoot(),
    DomainOrdersModule.forRoot(),
    DomainUsersModule.forRoot()
  ]
})
export class DomainsModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainsModule) {
    if (parentModule) {
      throw new Error(
        'DomainsModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainsModule,
      providers: [
        AccountFacade,
        AuthFacade,
        CartFacade,
        CategoriesFacade,
        DishesFacade,
        OrdersFacade,
        UsersFacade
      ]
    };
  }
}
