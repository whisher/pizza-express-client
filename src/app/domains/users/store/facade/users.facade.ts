import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Observable } from 'rxjs';

import { User } from '../../models';
import { UsersState } from '../reducers';
import {
  selectUserById,
  selectUserLoading,
  selectUsersAll,
  selectUsersError,
  selectUsersLoaded,
  selectUsersEntities,
  selectUsersTotal
} from '../selectors';

import {
  AddUser,
  DeleteUser,
  LoadUser,
  ResetUser,
  UpdateUser
} from '../actions';

export type UserUpdate = Update<User>;

@Injectable()
export class UsersFacade {
  constructor(private store: Store<UsersState>) {}

  get users$(): Observable<User[]> {
    return this.store.pipe(select(selectUsersAll));
  }

  get error$(): Observable<any> {
    return this.store.pipe(select(selectUsersError));
  }

  get entities$(): Observable<{ [id: string]: User }> {
    return this.store.pipe(select(selectUsersEntities));
  }

  get loaded$(): Observable<boolean> {
    return this.store.pipe(select(selectUsersLoaded));
  }

  get loading$(): Observable<boolean> {
    return this.store.pipe(select(selectUserLoading));
  }

  get selected$(): Observable<User> {
    return this.store.pipe(select(selectUserById));
  }

  get total$(): Observable<number> {
    return this.store.pipe(select(selectUsersTotal));
  }

  add(user: User) {
    this.store.dispatch(new AddUser({ user }));
  }

  delete(id: any) {
    this.store.dispatch(new DeleteUser({ id }));
  }

  load(id: any) {
    this.store.dispatch(new LoadUser({ id }));
  }

  reset() {
    this.store.dispatch(new ResetUser());
  }

  update(user: User) {
    this.store.dispatch(new UpdateUser({ user }));
  }
}
