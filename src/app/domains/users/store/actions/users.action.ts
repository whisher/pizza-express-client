import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { User } from '../../models';

export const enum UsersActionTypes {
  LoadUsers = '[Users] Load Users',
  LoadUsersSuccess = '[Users] Load Users Success',
  LoadUsersFail = '[Users] Load Users Fail',
  LoadUser = '[Users] Load User',
  LoadUserSuccess = '[Users] Load User Success',
  LoadUserFail = '[Users] Load User Fail',
  AddUser = '[Users] Add User',
  AddUserSuccess = '[Users] Add User Success',
  AddUserFail = '[Users] Add User Fail',
  UpdateUser = '[Users] Update User',
  UpdateUserRedirect = '[Users] Update User Redirect',
  UpdateUserSuccess = '[Users] Update User Success',
  UpdateUserFail = '[Users] Update User Fail',
  DeleteUser = '[Users] Delete User',
  DeleteUserSuccess = '[Users] Delete User Success',
  DeleteUserFail = '[Users] Delete User Fail',
  ResetUser = '[Users] Reset User'
}

export class LoadUsers implements Action {
  readonly type = UsersActionTypes.LoadUsers;
}

export class LoadUsersFail implements Action {
  readonly type = UsersActionTypes.LoadUsersFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadUsersSuccess implements Action {
  readonly type = UsersActionTypes.LoadUsersSuccess;

  constructor(readonly payload: { users: User[] }) {}
}

export class LoadUser implements Action {
  readonly type = UsersActionTypes.LoadUser;
  constructor(readonly payload: { id: any }) {}
}

export class LoadUserFail implements Action {
  readonly type = UsersActionTypes.LoadUsersFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadUserSuccess implements Action {
  readonly type = UsersActionTypes.LoadUserSuccess;

  constructor(readonly payload: { user: User }) {}
}

export class AddUser implements Action {
  readonly type = UsersActionTypes.AddUser;

  constructor(readonly payload: { user: User }) {}
}

export class AddUserFail implements Action {
  readonly type = UsersActionTypes.AddUserFail;

  constructor(readonly payload: { error: any }) {}
}

export class AddUserSuccess implements Action {
  readonly type = UsersActionTypes.AddUserSuccess;

  constructor(readonly payload: { user: User }) {}
}

export class UpdateUser implements Action {
  readonly type = UsersActionTypes.UpdateUser;

  constructor(readonly payload: { user: User }) {}
}

export class UpdateUserFail implements Action {
  readonly type = UsersActionTypes.UpdateUserFail;

  constructor(readonly payload: { error: any }) {}
}

export class UpdateUserRedirect implements Action {
  readonly type = UsersActionTypes.UpdateUserRedirect;

  constructor(readonly payload: { id: any }) {}
}

export class UpdateUserSuccess implements Action {
  readonly type = UsersActionTypes.UpdateUserSuccess;

  constructor(readonly payload: { user: User }) {}
}

export class DeleteUser implements Action {
  readonly type = UsersActionTypes.DeleteUser;

  constructor(readonly payload: { id: any }) {}
}

export class DeleteUserFail implements Action {
  readonly type = UsersActionTypes.DeleteUserFail;

  constructor(readonly payload: { error: any }) {}
}

export class DeleteUserSuccess implements Action {
  readonly type = UsersActionTypes.DeleteUserSuccess;

  constructor(readonly payload: { id: any }) {}
}

export class ResetUser implements Action {
  readonly type = UsersActionTypes.ResetUser;
}

export type UsersActions =
  | LoadUsers
  | LoadUsersFail
  | LoadUsersSuccess
  | LoadUser
  | LoadUserFail
  | LoadUserSuccess
  | AddUser
  | AddUserFail
  | AddUserSuccess
  | UpdateUser
  | UpdateUserFail
  | UpdateUserRedirect
  | UpdateUserSuccess
  | DeleteUser
  | DeleteUserFail
  | DeleteUserSuccess
  | ResetUser;
