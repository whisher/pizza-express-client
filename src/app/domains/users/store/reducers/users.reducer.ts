import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { UsersActionTypes, UsersActions } from '../actions/users.action';
import { User } from '../../models';

export interface State extends EntityState<User> {
  error: boolean;
  loaded: boolean;
  loading: boolean;
  selectedUserId: string | null;
}

export function selectUserId(user: User): string {
  return user._id;
}

export function sortByName(user: User, userCompare: User): number {
  return user.firstname.localeCompare(userCompare.firstname);
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
  selectId: selectUserId,
  sortComparer: sortByName
});

export const initialState: State = adapter.getInitialState({
  error: false,
  loaded: false,
  loading: false,
  selectedUserId: null
});

export function reducer(state = initialState, action: UsersActions): State {
  switch (action.type) {
    case UsersActionTypes.LoadUsers: {
      return {
        ...state,
        loading: true
      };
    }

    case UsersActionTypes.LoadUsersSuccess: {
      const users = action.payload.users;
      const current = {
        ...state,
        loaded: true,
        loading: false
      };
      return adapter.addAll(users, current);
    }

    case UsersActionTypes.LoadUserSuccess: {
      const user = action.payload.user;
      const current = {
        ...state
      };
      return adapter.upsertOne(user, current);
    }

    case UsersActionTypes.LoadUsersFail: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }

    case UsersActionTypes.AddUser:
    case UsersActionTypes.UpdateUser: {
      return {
        ...state,
        loading: true
      };
    }

    case UsersActionTypes.AddUserFail:
    case UsersActionTypes.UpdateUserFail:
    case UsersActionTypes.DeleteUserFail: {
      const error = action.payload.error;
      return {
        ...state,
        error,
        loading: false,
        loaded: false
      };
    }

    case UsersActionTypes.AddUserSuccess: {
      return adapter.addOne(action.payload.user, state);
    }

    case UsersActionTypes.UpdateUserRedirect: {
      const selectedUserId = action.payload.id;
      return {
        ...state,
        selectedUserId
      };
    }

    case UsersActionTypes.UpdateUserSuccess: {
      const user = action.payload.user;
      return adapter.updateOne({ id: user._id, changes: user }, state);
    }

    case UsersActionTypes.DeleteUserSuccess: {
      const id = action.payload.id;
      return adapter.removeOne(action.payload.id, state);
    }

    case UsersActionTypes.ResetUser: {
      return {
        ...state,
        error: false
      };
    }
  }

  return state;
}

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

// select the array of user ids
export const selectUsersIds = selectIds;

// select the dictionary of user entities
export const selectUsersEntities = selectEntities;

// select the array of users
export const selectUsersAll = selectAll;

// select the total user count
export const selectUsersTotal = selectTotal;

export const getSelectedUserId = (state: State) => state.selectedUserId;
export const selectUsersLoaded = (state: State) => state.loaded;
export const selectUsersError = (state: State) => state.error;
export const selectUsersLoading = (state: State) => state.loading;
