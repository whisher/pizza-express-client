import {
  createSelector,
  createFeatureSelector,
  ActionReducerMap
} from '@ngrx/store';

import * as fromRouter from '../../../router';
import { UsersState } from '../reducers';
import * as fromUser from '../reducers/users.reducer';

export const selectUserStatus = createFeatureSelector<UsersState>('users');
export const selectUserState = createSelector(
  selectUserStatus,
  (state: UsersState) => {
    return state.users;
  }
);

export const selectUserIds = createSelector(
  selectUserState,
  fromUser.selectUsersIds
);

export const selectUsersEntities = createSelector(
  selectUserState,
  fromUser.selectUsersEntities
);

export const selectUsersAll = createSelector(
  selectUserState,
  fromUser.selectUsersAll
);

export const selectUsersTotal = createSelector(
  selectUserState,
  fromUser.selectUsersTotal
);

export const selectUserById = createSelector(
  selectUsersEntities,
  fromRouter.selectRouterState,
  (entities, router) => {
    return router.state && entities[router.state.params.id];
  }
);

export const selectUsersError = createSelector(
  selectUserState,
  fromUser.selectUsersError
);

export const selectUsersLoaded = createSelector(
  selectUserState,
  fromUser.selectUsersLoaded
);

export const selectUserLoading = createSelector(
  selectUserState,
  fromUser.selectUsersLoading
);
