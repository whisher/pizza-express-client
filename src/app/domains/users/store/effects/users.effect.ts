import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { User } from '../../models';
import { Go } from '../../../router';
import * as actions from '../actions';

import * as fromServices from '../../services';

@Injectable()
export class UsersEffects {
  @Effect()
  loadUsers$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.LoadUsers),
    switchMap(() => {
      return this.service.load().pipe(
        map(users => new actions.LoadUsersSuccess({ users })),
        catchError(error => of(new actions.LoadUsersFail({ error })))
      );
    })
  );

  @Effect()
  loadUser$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.LoadUser),
    map((action: actions.LoadUser) => {
      return new Go({ path: [`/admin/users/edit/${action.payload.id}`] });
    })
  );

  @Effect()
  addUser$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.AddUser),
    map((action: actions.AddUser) => action.payload.user),
    switchMap((_user: User) => {
      return this.service.add(_user).pipe(
        map((user: User) => new actions.AddUserSuccess({ user })),
        catchError(error => of(new actions.AddUserFail({ error })))
      );
    })
  );

  @Effect()
  addUserSuccess$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.AddUserSuccess),
    map((action: actions.AddUserSuccess) => action.payload.user),
    map((user: User) => {
      return new Go({ path: ['/admin/users'] });
    })
  );

  @Effect()
  updateUser$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.UpdateUser),
    map((action: actions.UpdateUser) => action.payload.user),
    switchMap((_user: User) => {
      return this.service.update(_user._id, _user).pipe(
        map(user => new actions.UpdateUserSuccess({ user })),
        catchError(error => of(new actions.UpdateUserFail({ error })))
      );
    })
  );

  @Effect()
  updateUsersSuccess$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.UpdateUserSuccess),
    map((action: actions.UpdateUserSuccess) => action.payload.user),
    map((user: any) => new Go({ path: ['/admin/users'] }))
  );

  @Effect()
  deleteUser$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.DeleteUser),
    map((action: actions.DeleteUser) => action.payload.id),
    switchMap(_id => {
      return this.service.delete(_id).pipe(
        map((id: any) => new actions.DeleteUserSuccess({ id })),
        catchError(error => of(new actions.DeleteUserFail({ error })))
      );
    })
  );

  @Effect()
  deleteUsersSuccess$ = this.actions$.pipe(
    ofType(actions.UsersActionTypes.DeleteUserSuccess),
    map((action: actions.DeleteUserSuccess) => {
      return new Go({
        path: ['/admin/users']
      });
    })
  );

  constructor(
    private actions$: Actions,
    private service: fromServices.UsersService
  ) {}
}
