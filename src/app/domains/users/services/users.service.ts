import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { httpErrorHandler } from '../../../shared/utils';
import { User } from '../models';

@Injectable()
export class UsersService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.users;
  }

  add(data: User): Observable<User> {
    return this.http
      .post<User>(this.apiUrl, data)
      .pipe(catchError(httpErrorHandler));
  }

  load(): Observable<User[]> {
    return this.http
      .get<User[]>(this.apiUrl)
      .pipe(catchError(httpErrorHandler));
  }

  findById(id: string): Observable<User> {
    return this.http
      .get<User>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }

  update(id: any, user: User): Observable<User> {
    return this.http
      .put<User>(`${this.apiUrl}/${id}`, user)
      .pipe(catchError(httpErrorHandler));
  }

  delete(id: string): Observable<User> {
    return this.http
      .delete<User>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }
}
