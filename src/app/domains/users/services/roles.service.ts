import { Injectable } from '@angular/core';

import { roles } from '../models';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  getRoles(): Array<roles> {
    return ['admin', 'customer', 'manager', 'user'];
  }
}
