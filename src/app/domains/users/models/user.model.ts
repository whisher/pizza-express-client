export interface User {
  _id?: string;
  avatar?: string;
  created: number;
  email: string;
  firstname: string;
  lastname: string;
  mobile: string;
  password: string;
  role: roles;
}

export type roles = 'admin' | 'customer' | 'manager' | 'user';
