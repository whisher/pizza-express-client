import { Injectable } from '@angular/core';

import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, take, switchMap, catchError, filter } from 'rxjs/operators';

import * as fromStore from '../store';

@Injectable()
export class UsersGuard implements CanActivate {
  constructor(private store: Store<fromStore.UsersState>) {}

  canActivate(): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.store.select(fromStore.selectUsersLoaded).pipe(
      filter(loaded => loaded),
      take(1)
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.selectUsersLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromStore.LoadUsers());
        }
      }),
      take(1)
    );
  }
}
