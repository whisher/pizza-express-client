// Core
import {
  NgModule,
  ModuleWithProviders,
  Optional,
  SkipSelf
} from '@angular/core';

// Libs
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Store
import { reducers, effects } from './store';
import * as fromService from './services';
import * as fromGuards from './guards';

@NgModule({
  imports: [
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature(effects)
  ]
})
export class DomainUsersModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainUsersModule) {
    if (parentModule) {
      throw new Error(
        'DomainUsersModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainUsersModule,
      providers: [...fromGuards.guards, ...fromService.services]
    };
  }
}
