import * as fromRouterActions from './router.actions';

// Mocks
const withOnlyRequiredParams = {
  path: ['/admim']
};
const withAllParams = {
  path: ['/admim'],
  queryParams: { page: 1 },
  extras: { fragment: 'top' }
};
describe('Router Actions', () => {
  describe('Go', () => {
    it('should create an action with only required params', () => {
      const action = new fromRouterActions.Go(withOnlyRequiredParams);
      const payload = { ...withOnlyRequiredParams };
      expect({ ...action }).toEqual({
        type: fromRouterActions.RouterActionTypes.Go,
        payload
      });
    });
    it('should create an action with all params', () => {
      const action = new fromRouterActions.Go(withAllParams);
      const payload = { ...withAllParams };
      expect({ ...action }).toEqual({
        type: fromRouterActions.RouterActionTypes.Go,
        payload
      });
    });
  });
  describe('Back', () => {
    it('should create an action', () => {
      const action = new fromRouterActions.Back();
      expect({ ...action }).toEqual({
        type: fromRouterActions.RouterActionTypes.Back
      });
    });
  });
  describe('Forward', () => {
    it('should create an action', () => {
      const action = new fromRouterActions.Forward();
      expect({ ...action }).toEqual({
        type: fromRouterActions.RouterActionTypes.Forward
      });
    });
  });
});
