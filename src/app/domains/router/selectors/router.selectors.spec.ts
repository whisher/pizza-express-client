// Core
import { Component } from '@angular/core';

// Testing
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

// Ngrx
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';
import {
  NavigationActionTiming,
  StoreRouterConnectingModule
} from '@ngrx/router-store';

// State
import { CustomRouterStateSerializer } from '../router.state.serializer';
import { reducer } from '../reducers/router.reducer';
import { RouterEffects } from '../effects/router.effects';
import { RouterState } from '../router.state';
import * as fromActions from '../actions/router.actions';
import * as fromSelectors from './router.selectors';

// Mocks
@Component({
  selector: 'admin',
  template: ''
})
class MockAdminComponent {}

describe('Router Selectors', () => {
  let store: Store<RouterState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'admin', component: MockAdminComponent }
        ]),
        StoreModule.forRoot(reducer),
        EffectsModule.forRoot([RouterEffects]),
        StoreRouterConnectingModule.forRoot({
          stateKey: 'router',
          serializer: CustomRouterStateSerializer,
          navigationActionTiming: NavigationActionTiming.PostActivation
        })
      ],
      declarations: [MockAdminComponent]
    });
    store = TestBed.get(Store);
    store.dispatch(new fromActions.Go({ path: ['/admin'] }));
  }));

  describe('getRouterState', () => {
    it('should return params of the given route', fakeAsync(() => {
      const params = { url: '/admin', params: {}, queryParams: {} };
      store.select(fromSelectors.selectRouterParams).subscribe(result => {
        expect(result).toEqual(params);
      });
      tick();
    }));
  });
});
