// Testing
import { TestBed } from '@angular/core/testing';

// Ngrx
import { combineReducers, select, StoreModule, Store } from '@ngrx/store';

// Store
import { AuthToken } from '../models';
import { reducer } from '../../router';
import {
  AuthState,
  authReducer,
  LoginFailure,
  LoginJustLogged,
  LoginReset,
  LoginSuccess,
  selectError
} from '../store';

// Guard
import { AuthLoginGuard } from './auth.guard.login';

// Mocks
const error: any = { status: 401, message: '401 Unauthorized Error' };
const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};
describe('AuthLoginGuard', () => {
  let store: Store<AuthState>;
  let guard: AuthLoginGuard;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ],
      providers: [Store, AuthLoginGuard]
    });
    store = TestBed.get(Store);
    guard = TestBed.get(AuthLoginGuard);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should canActivate return true and dispatch LoginReset action', () => {
    const result = guard.canActivate();
    result.subscribe(isActivable => {
      expect(isActivable).toBe(true);
    });
    expect(store.dispatch).toHaveBeenCalledWith(new LoginReset());
  });

  it('should canActivate return true and dispatch LoginReset', () => {
    store.dispatch(new LoginFailure({ error }));
    const result = guard.canActivate();
    result.subscribe(isActivable => {
      expect(isActivable).toBe(true);
    });
    store.pipe(select(selectError)).subscribe(_error => {
      expect(_error).toBe(null);
    });
    expect(store.dispatch).toHaveBeenCalledWith(new LoginReset());
  });

  it('should canActivate return false and dispatch LoginJustLogged action', () => {
    store.dispatch(new LoginSuccess({ token }));
    const result = guard.canActivate();
    result.subscribe(isLoadable => {
      expect(isLoadable).toBe(false);
    });
    expect(store.dispatch).toHaveBeenCalledWith(new LoginJustLogged());
  });
});
