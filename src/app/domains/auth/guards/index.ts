import { AuthGuard } from './auth.guard';
import { AuthLoginGuard } from './auth.guard.login';

export const guards: any[] = [AuthGuard, AuthLoginGuard];

export * from './auth.guard';
export * from './auth.guard.login';
