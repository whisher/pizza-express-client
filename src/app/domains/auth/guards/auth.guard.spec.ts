// Testing
import { TestBed } from '@angular/core/testing';

// Ngrx
import { StoreModule, Store, combineReducers } from '@ngrx/store';

// Store
import { AuthToken } from '../models';
import { reducer } from '../../router';
import { AuthState, authReducer, Logout, LoginSuccess } from '../store';

// Guard
import { AuthGuard } from './auth.guard';

// Mocks
const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};
describe('AuthGuard', () => {
  let store: Store<AuthState>;
  let guard: AuthGuard;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ],
      providers: [Store, AuthGuard]
    });
    store = TestBed.get(Store);
    guard = TestBed.get(AuthGuard);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should canActivate return false and dispatch logout action', () => {
    const result = guard.canActivate();
    result.subscribe(isActivable => {
      expect(isActivable).toBe(false);
    });
    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });

  it('should canLoad return false and dispatch logout action', () => {
    const result = guard.canLoad();
    result.subscribe(isLoadable => {
      expect(isLoadable).toBe(false);
    });
    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });

  it('should canActivate return true and not dispatch logout action', () => {
    store.dispatch(new LoginSuccess({ token }));
    const canActivate = guard.canActivate();
    canActivate.subscribe(isActivable => {
      expect(isActivable).toBe(true);
    });
    const canLoad = guard.canLoad();
    canActivate.subscribe(isLoadable => {
      expect(isLoadable).toBe(true);
    });
    expect(store.dispatch).not.toHaveBeenCalledWith(new Logout());
  });
});
