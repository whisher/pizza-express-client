import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { AuthToken, Credentials } from '../models';
import { User } from '../../users';
import { httpErrorHandler } from '../../../shared/utils';

@Injectable()
export class AuthService {
  apiUrlLogin: string;
  apiUrlSignup: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrlLogin = config.api.auth.login;
    this.apiUrlSignup = config.api.auth.signup;
  }

  signup(data: User): Observable<AuthToken> {
    return this.http
      .post<AuthToken>(this.apiUrlSignup, data)
      .pipe(catchError(httpErrorHandler));
  }

  login(data: Credentials): Observable<AuthToken> {
    return this.http
      .post<AuthToken>(this.apiUrlLogin, data)
      .pipe(catchError(httpErrorHandler));
  }
}
