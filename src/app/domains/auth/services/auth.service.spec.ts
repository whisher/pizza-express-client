import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CONFIG, Config } from '../../../core/config';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        {
          provide: CONFIG,
          useClass: Config
        }
      ]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(AuthService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return error with message', () => {
    const error = new HttpErrorResponse({
      status: 401,
      statusText: 'Not Found'
    });

    service.login({ email: 'tu@tu.tu', password: 'abc' }).subscribe(
      data => {},
      err => {
        expect(err).toEqual('Something bad happened; please try again later.');
      }
    );

    const req = httpTestingController.expectOne(service.apiLogin);
    expect(req.request.method).toBe('POST');
    req.flush(null, error);
  });

  it('should be return token', () => {
    const token = {
      token: 'abc',
      expiresIn: 123
    };
    service.login({ email: 'io@io.io', password: 'abc' }).subscribe(data => {
      expect(data).toEqual(token);
    });
    const req = httpTestingController.expectOne(service.apiLogin);
    req.flush(token);
  });
});
