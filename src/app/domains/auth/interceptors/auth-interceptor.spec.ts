// Core
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Testing
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

// Ngrx
import { StoreModule, Store, combineReducers } from '@ngrx/store';

// Store
import { AuthToken } from '../models';
import { reducer } from '../../router';
import { AuthState, authReducer, LoginSuccess } from '../store';

// Interceptor
import { AuthInterceptor } from './auth-interceptor';

// Mocks
interface Data {
  name: string;
}
const testUrl = '/data';
const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};

describe('AuthInterceptor', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let store: Store<AuthState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ],
      providers: [
        Store,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
      ]
    });
    store = TestBed.get(Store);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should Authorization to be false', () => {
    const testData: Data = { name: 'Test Data' };
    httpClient.get<Data>(testUrl).subscribe(data => {
      expect(data).toEqual(testData);
    });

    const httpRequest = httpTestingController.expectOne('/data');
    expect(httpRequest.request.headers.has('Authorization')).toEqual(false);

    httpRequest.flush(testData);
    httpTestingController.verify();
  });

  it('should Authorization to be true', () => {
    store.dispatch(new LoginSuccess({ token }));
    const testData: Data = { name: 'Test Data' };
    httpClient.get<Data>(testUrl).subscribe(data => {
      expect(data).toEqual(testData);
    });

    const httpRequest = httpTestingController.expectOne('/data');
    expect(httpRequest.request.headers.has('Authorization')).toBe(true);
    expect(httpRequest.request.headers.get('Authorization')).toEqual(
      token.token
    );
    httpRequest.flush(testData);
    httpTestingController.verify();
  });
});
