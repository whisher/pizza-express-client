// Core
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Testing
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

// Ngrx
import { StoreModule, Store, combineReducers } from '@ngrx/store';

// Store
import { AuthToken } from '../models';
import { reducer } from '../../router';
import { AuthState, authReducer, Logout, LoginSuccess } from '../store';

// Interceptor
import { AuthErrorInterceptor } from './auth-error-interceptor';

// Mocks
interface Data {
  name: string;
}
const testUrl = '/data';
const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};

describe('AuthErrorInterceptor', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let store: Store<AuthState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ],
      providers: [
        Store,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthErrorInterceptor,
          multi: true
        }
      ]
    });
    store = TestBed.get(Store);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    spyOn(store, 'dispatch').and.callThrough();
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should not dispatch logout without token', () => {
    const testData: Data = { name: 'Test Data' };
    httpClient.get<Data>(testUrl).subscribe(data => {
      expect(data).toEqual(testData);
    });

    const httpRequest = httpTestingController.expectOne('/data');
    httpRequest.flush(testData);
    expect(store.dispatch).not.toHaveBeenCalledWith(new Logout());
  });

  it('should not dispatch logout with token', () => {
    store.dispatch(new LoginSuccess({ token }));
    const testData: Data = { name: 'Test Data' };
    httpClient.get<Data>(testUrl).subscribe(data => {
      expect(data).toEqual(testData);
    });

    const httpRequest = httpTestingController.expectOne('/data');
    httpRequest.flush(testData);
    expect(store.dispatch).not.toHaveBeenCalledWith(new Logout());
  });

  it('should dispatch logout with 404 status', () => {
    const emsg = 'deliberate 404 error';

    httpClient.get<Data[]>(testUrl).subscribe(
      data => fail('should have failed with the 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(404, 'status');
        expect(error.error).toEqual(emsg, 'message');
      }
    );

    const req = httpTestingController.expectOne(testUrl);
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });

  it('should dispatch logout with 403 status', () => {
    const emsg = 'deliberate 403 error';

    httpClient.get<Data[]>(testUrl).subscribe(
      data => fail('should have failed with the 403 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(403, 'status');
        expect(error.error).toEqual(emsg, 'message');
      }
    );

    const req = httpTestingController.expectOne(testUrl);
    req.flush(emsg, { status: 403, statusText: 'Forbidden' });
    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });

  it('should dispatch logout with 401 status', () => {
    const emsg = 'deliberate 401 error';

    httpClient.get<Data[]>(testUrl).subscribe(
      data => fail('should have failed with the 401 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(401, 'status');
        expect(error.error).toEqual(emsg, 'message');
      }
    );

    const req = httpTestingController.expectOne(testUrl);
    req.flush(emsg, { status: 401, statusText: 'Unauthorized' });
    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });
});
