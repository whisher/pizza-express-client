export interface Credentials {
  email: string;
  password: string;
}

export interface AuthToken {
  token: string;
  expiresIn: number;
  expiredAt?: Date;
}
