// Testing
import { TestBed } from '@angular/core/testing';

// Ngrx
import { StoreModule, Store, combineReducers } from '@ngrx/store';

// Store
import { Credentials, AuthToken } from '../../models';
import { reducer } from '../../../router';
import {
  AuthState,
  authReducer,
  Logout,
  Login,
  LoginFailure,
  LoginSuccess
} from '../';

// Guard
import { AuthFacade } from './auth.facade';

// Mocks
const credentials: Credentials = {
  email: 'io@io.ii',
  password: 'abc'
};

const error: any = { status: 401, message: '401 Unauthorized Error' };

const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};

describe('AuthFacade', () => {
  let store: Store<AuthState>;
  let facade: AuthFacade;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ],
      providers: [Store, AuthFacade]
    });
    store = TestBed.get(Store);
    facade = TestBed.get(AuthFacade);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should have properties', () => {
    facade.error$.subscribe(_error => {
      expect(_error).toBe(null);
    });
    facade.loading$.subscribe(loading => {
      expect(loading).toBe(false);
    });
    facade.isAuthenticated$.subscribe(isAuthenticated => {
      expect(isAuthenticated).toBe(false);
    });
    facade.token$.subscribe(_token => {
      expect(_token).toBe(null);
    });
  });

  it('should token update when dispatch LoginSuccess', () => {
    store.dispatch(new LoginSuccess({ token }));

    facade.token$.subscribe(_token => {
      expect(_token).toEqual(token);
    });
  });

  it('should error update when dispatch LoginSuccess', () => {
    store.dispatch(new LoginFailure({ error }));

    facade.error$.subscribe(_error => {
      expect(_error).toEqual(error);
    });
  });

  it('should login dispatchs login action', () => {
    facade.login(credentials);
    expect(store.dispatch).toHaveBeenCalledWith(new Login({ credentials }));
    facade.loading$.subscribe(loading => {
      expect(loading).toBe(true);
    });
  });

  it('should login dispatchs login action', () => {
    facade.logout();

    facade.token$.subscribe(_token => {
      expect(_token).toEqual(null);
    });

    expect(store.dispatch).toHaveBeenCalledWith(new Logout());
  });
});
