import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { Credentials } from '../../models';
import { User } from '../../../users';
import { State } from '../reducers';
import {
  selectError,
  selectLoading,
  selectIsAuthenticated,
  selectToken
} from '../selectors';

import { Login, Logout, Signup } from '../actions';

@Injectable()
export class AuthFacade {
  error$ = this.store.pipe(select(selectError));
  loading$ = this.store.pipe(select(selectLoading));
  isAuthenticated$ = this.store.pipe(select(selectIsAuthenticated));
  token$ = this.store.pipe(select(selectToken));
  constructor(private store: Store<State>) {}

  login(credentials: Credentials): void {
    this.store.dispatch(new Login({ credentials }));
  }

  signup(user: User): void {
    this.store.dispatch(new Signup({ user }));
  }

  logout(): void {
    this.store.dispatch(new Logout());
  }
}
