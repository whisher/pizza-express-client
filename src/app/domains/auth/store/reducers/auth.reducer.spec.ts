import { Credentials, AuthToken } from '../../models';
import * as fromAuthActions from '../actions/auth.actions';
import * as fromAuthReducer from './auth.reducer';

const credentials: Credentials = {
  email: 'io@io.ii',
  password: 'abc'
};
const error: any = { status: 401, message: '401 Unauthorized Error' };
const token: AuthToken = {
  token: 'abc',
  expiresIn: 3000
};
const {
  initialState,
  getError,
  getIsAuthenticated,
  getLoading,
  getToken,
  reducer
} = fromAuthReducer;

describe('AuthReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const state = reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });
  describe('login action', () => {
    it('should return loading true', () => {
      const action = new fromAuthActions.Login({ credentials });
      const state = reducer(initialState, action);

      expect(state.error).toBe(null);
      expect(state.loading).toBe(true);
      expect(state.token).toBe(null);
      expect(getLoading(state)).toBe(true);
    });
  });
  describe('login failure action', () => {
    it('should return error', () => {
      const action = new fromAuthActions.LoginFailure({ error });
      const state = reducer(initialState, action);

      expect(state.error).toEqual(error);
      expect(state.loading).toBe(false);
      expect(state.token).toBe(null);
      expect(getError(state)).toEqual(error);
    });
  });
  describe('login success action', () => {
    it('should return error', () => {
      const action = new fromAuthActions.LoginSuccess({ token });
      const state = reducer(initialState, action);

      expect(state.error).toBe(null);
      expect(state.loading).toBe(false);
      expect(state.token).toEqual(token);
      expect(getToken(state)).toEqual(token);
      expect(getIsAuthenticated(state)).toBe(true);
    });
  });
  describe('logout action', () => {
    it('should return the default state', () => {
      const action = new fromAuthActions.Logout();
      const state = reducer({ token, loading: false, error: null }, action);

      expect(state).toBe(initialState);
      expect(getIsAuthenticated(state)).toBe(false);
    });
  });
});
