import { Credentials, AuthToken } from '../../models';
import * as fromAuthActions from './auth.actions';

const credentials: Credentials = {
  email: 'io@io.ii',
  password: 'abc'
};
const error: any = { status: 401, message: '401 Unauthorized Error' };
const token: AuthToken = {
  token: 'abc',
  expiresIn: 1546356028904
};

describe('Auth Actions', () => {
  describe('Login', () => {
    it('should create an action', () => {
      const action = new fromAuthActions.Login({ credentials });
      const payload = { credentials };
      expect({ ...action }).toEqual({
        type: fromAuthActions.AuthActionTypes.Login,
        payload
      });
    });
  });

  describe('LoginFailure', () => {
    it('should create an action', () => {
      const action = new fromAuthActions.LoginFailure({ error });
      const payload = { error };
      expect({ ...action }).toEqual({
        type: fromAuthActions.AuthActionTypes.LoginFailure,
        payload
      });
    });
  });

  describe('LoginSuccess', () => {
    it('should create an action', () => {
      const action = new fromAuthActions.LoginSuccess({ token });
      const payload = { token };
      expect({ ...action }).toEqual({
        type: fromAuthActions.AuthActionTypes.LoginSuccess,
        payload
      });
    });
  });

  describe('Logout', () => {
    it('should create an action', () => {
      const action = new fromAuthActions.Logout();
      expect({ ...action }).toEqual({
        type: fromAuthActions.AuthActionTypes.Logout
      });
    });
  });
});
