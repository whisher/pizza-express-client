import { Action } from '@ngrx/store';
import { Credentials, AuthToken } from '../../models';
import { User } from '../../../users';

export enum AuthActionTypes {
  Login = '[Auth] Page Login',
  LoginFailure = '[Auth] Api Login Failure',
  LoginJustLogged = '[Auth] Guard Login Just Logged',
  LoginReset = '[Auth] Guard Login Reset',
  LoginSuccess = '[Auth] Api Login Success',
  Logout = '[Auth] Page Logout',
  LogoutFailure = '[Auth] Api Logout Failure',
  LogoutSuccess = '[Auth] Api Logout Success',
  Signup = '[Auth] Page signup',
  SignupFailure = '[Auth] Api Signup Failure',
  SignupSuccess = '[Auth] Api Signup Success'
}

export class Login implements Action {
  readonly type = AuthActionTypes.Login;
  constructor(readonly payload: { credentials: Credentials }) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;
  constructor(readonly payload: { error: any }) {}
}

export class LoginJustLogged implements Action {
  readonly type = AuthActionTypes.LoginJustLogged;
}

export class LoginReset implements Action {
  readonly type = AuthActionTypes.LoginReset;
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;
  constructor(readonly payload: { token: AuthToken }) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export class Signup implements Action {
  readonly type = AuthActionTypes.Signup;
  constructor(readonly payload: { user: User }) {}
}

export class SignupFailure implements Action {
  readonly type = AuthActionTypes.SignupFailure;
  constructor(readonly payload: { error: any }) {}
}

export class SignupSuccess implements Action {
  readonly type = AuthActionTypes.SignupSuccess;
  constructor(readonly payload: { token: AuthToken }) {}
}

export type AuthActions =
  | Login
  | LoginFailure
  | LoginJustLogged
  | LoginReset
  | LoginSuccess
  | Logout
  | Signup
  | SignupFailure
  | SignupSuccess;
