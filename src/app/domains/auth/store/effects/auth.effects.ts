import { Injectable } from '@angular/core';

import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { Actions, Effect, ofType } from '@ngrx/effects';

import { Credentials, AuthToken } from '../../models';
import { User } from '../../../users';

import { Go } from '../../../router';
import { AuthService } from '../../services';
import {
  AuthActionTypes,
  Login,
  LoginFailure,
  LoginSuccess,
  Signup,
  SignupFailure,
  SignupSuccess
} from '../actions';

@Injectable()
export class AuthEffects {
  @Effect()
  signup$ = this.actions$.pipe(
    ofType(AuthActionTypes.Signup),
    map((action: Signup) => action.payload.user),
    switchMap((user: User) => {
      return this.service.signup(user).pipe(
        map((token: AuthToken) => new SignupSuccess({ token })),
        catchError(error => of(new SignupFailure({ error })))
      );
    })
  );

  @Effect()
  signupSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.SignupSuccess, AuthActionTypes.LoginJustLogged),
    map(() => new Go({ path: ['/'] }))
  );

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthActionTypes.Login),
    map((action: Login) => action.payload.credentials),
    switchMap((credentials: Credentials) => {
      return this.service.login(credentials).pipe(
        map((token: AuthToken) => new LoginSuccess({ token })),
        catchError(error => of(new LoginFailure({ error })))
      );
    })
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess, AuthActionTypes.LoginJustLogged),
    map(() => new Go({ path: ['/auth/account'] }))
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    map(() => {
      return new Go({ path: ['/auth'], extras: { replaceUrl: true } });
    })
  );

  constructor(private actions$: Actions, private service: AuthService) {}
}
