// Testing
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';

// Rxjs
import { Observable } from 'rxjs';

// Marbles
import { hot, cold } from 'jasmine-marbles';

// Store
import { Credentials, AuthToken } from '../../models';
import { CONFIG, Config } from '../../../../core/config';
import { AuthService } from '../../services';
import { AuthEffects } from './auth.effects';
import * as fromActions from '../actions';

const credentials: Credentials = {
  email: 'io@io.io',
  password: 'abc'
};
const error: any = { status: 401, message: '401 Unauthorized Error' };
const token: AuthToken = {
  token: 'abc',
  expiresIn: 1546356028904
};

xdescribe('AuthEffects', () => {
  let effects: AuthEffects;
  let actions: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        {
          provide: CONFIG,
          useClass: Config
        },
        AuthEffects,
        provideMockActions(() => actions)
      ]
    });

    effects = TestBed.get(AuthEffects);
  });

  it('should work', () => {
    const action = new fromActions.Login({ credentials });
    const completion = new fromActions.LoginSuccess({ token });

    // Refer to 'Writing Marble Tests' for details on '--a-' syntax
    actions = hot('a', { a: action });
    const expected = cold('-b', { b: completion });

    expect(effects.login$).toBeObservable(expected);
  });
});
