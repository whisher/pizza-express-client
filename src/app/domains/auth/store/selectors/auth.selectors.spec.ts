// Testing
import { TestBed } from '@angular/core/testing';

// Ngrx
import { combineReducers, Store, StoreModule, select } from '@ngrx/store';

// Stores
import { Credentials, AuthToken } from '../../models';
import { reducer } from '../../../router';
import { AuthState, authReducer, State } from '../reducers';
import * as fromActions from '../actions';
import * as fromSelectors from './auth.selectors';

// Mock
const state: State = {
  error: null,
  loading: false,
  token: null
};

describe('Auth Selectors', () => {
  let store: Store<AuthState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...reducer,
          auth: combineReducers(authReducer)
        })
      ]
    });

    store = TestBed.get(Store);
  });

  describe('selectAuthStatus', () => {
    it('should return initial state', () => {
      store.pipe(select(fromSelectors.selectAuthStatus)).subscribe(result => {
        expect(result).toEqual(state);
      });
    });
  });

  describe('selectError', () => {
    it('should return the error', () => {
      const error: any = { status: 401, message: '401 Unauthorized Error' };
      store.dispatch(new fromActions.LoginFailure({ error }));
      store.pipe(select(fromSelectors.selectError)).subscribe(result => {
        expect(result).toEqual(error);
      });
    });
  });

  describe('selectLoading', () => {
    it('should return loading true', () => {
      const credentials: Credentials = { email: 'io@io.io', password: 'abc' };
      store.dispatch(new fromActions.Login({ credentials }));
      store.pipe(select(fromSelectors.selectLoading)).subscribe(result => {
        expect(result).toBe(true);
      });
    });
  });

  describe('selectIsAuthenticated', () => {
    it('should return selectIsAuthenticated false', () => {
      store
        .pipe(select(fromSelectors.selectIsAuthenticated))
        .subscribe(result => {
          expect(result).toBe(false);
        });
    });
  });

  describe('selectToken selectIsAuthenticated', () => {
    it('should return the token and selectIsAuthenticated true', () => {
      const token: AuthToken = {
        token: 'abc',
        expiresIn: 3000
      };
      store.dispatch(new fromActions.LoginSuccess({ token }));
      store.pipe(select(fromSelectors.selectToken)).subscribe(result => {
        expect(result).toEqual(token);
      });
      store
        .pipe(select(fromSelectors.selectIsAuthenticated))
        .subscribe(result => {
          expect(result).toBe(true);
        });
    });
  });
});
