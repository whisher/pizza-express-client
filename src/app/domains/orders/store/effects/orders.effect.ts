import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Update } from '@ngrx/entity';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Order } from '../../models';
import { Go } from '../../../router';
import * as actions from '../actions';

import * as fromServices from '../../services';

@Injectable()
export class OrdersEffects {
  @Effect()
  loadOrders$ = this.actions$.pipe(
    ofType(actions.OrdersActionTypes.LoadOrders),
    switchMap(() => {
      return this.service.load().pipe(
        map((orders: Order[]) => new actions.LoadOrdersSuccess({ orders })),
        catchError(error => of(new actions.LoadOrdersFail({ error })))
      );
    })
  );

  @Effect()
  loadOrder$ = this.actions$.pipe(
    ofType(actions.OrdersActionTypes.LoadOrder),
    map((action: actions.LoadOrder) => {
      return new Go({ path: [`/admin/orderes/edit/${action.payload.id}`] });
    })
  );

  @Effect()
  addOrder$ = this.actions$.pipe(
    ofType(actions.OrdersActionTypes.AddOrder),
    map((action: actions.AddOrder) => action.payload.order),
    switchMap((_order: Order) => {
      return this.service.add(_order).pipe(
        map((order: Order) => new actions.AddOrderSuccess({ order })),
        catchError(error => of(new actions.AddOrderFail({ error })))
      );
    })
  );

  @Effect()
  addOrUpdateOrderSuccess$ = this.actions$.pipe(
    ofType(
      actions.OrdersActionTypes.AddOrderSuccess,
      actions.OrdersActionTypes.UpdateOrderSuccess
    ),
    map(() => new Go({ path: ['/admin/orderes'] }))
  );

  @Effect()
  updateOrder$ = this.actions$.pipe(
    ofType(actions.OrdersActionTypes.UpdateOrder),
    map((action: actions.UpdateOrder) => action.payload.order),
    switchMap((_order: Order) => {
      return this.service.update(_order._id, _order).pipe(
        map((order: Order) => new actions.UpdateOrderSuccess({ order })),
        catchError(error => of(new actions.UpdateOrderFail({ error })))
      );
    })
  );

  @Effect()
  deleteOrder$ = this.actions$.pipe(
    ofType(actions.OrdersActionTypes.DeleteOrder),
    map((action: actions.DeleteOrder) => action.payload.id),
    switchMap(_id => {
      return this.service.delete(_id).pipe(
        map((id: any) => new actions.DeleteOrderSuccess({ id })),
        catchError(error => of(new actions.DeleteOrderFail({ error })))
      );
    })
  );
  constructor(
    private actions$: Actions,
    private service: fromServices.OrdersService
  ) {}
}
