import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as fromRouter from '../../../router';
import { OrdersState } from '../reducers';
import * as fromOrder from '../reducers/orders.reducer';

export const selectOrderStatus = createFeatureSelector<OrdersState>('orders');
export const selectOrderState = createSelector(
  selectOrderStatus,
  (state: OrdersState) => {
    return state.orders;
  }
);

export const selectOrdersIds = createSelector(
  selectOrderState,
  fromOrder.selectOrdersIds
);

export const selectOrdersEntities = createSelector(
  selectOrderState,
  fromOrder.selectOrdersEntities
);

export const selectOrderById = createSelector(
  selectOrdersEntities,
  fromRouter.selectRouterState,
  (entities, router) => {
    return router.state && entities[router.state.params.id];
  }
);

export const selectOrdersAll = createSelector(
  selectOrderState,
  fromOrder.selectOrdersAll
);

export const selectOrdersError = createSelector(
  selectOrderState,
  fromOrder.selectOrdersError
);

export const selectOrdersLoaded = createSelector(
  selectOrderState,
  fromOrder.selectOrdersLoaded
);

export const selectOrdersLoading = createSelector(
  selectOrderState,
  fromOrder.selectOrdersLoading
);
