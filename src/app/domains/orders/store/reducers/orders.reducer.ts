import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { OrdersActionTypes, OrdersActions } from '../actions/orders.action';
import { Order } from '../../models';

export interface State extends EntityState<Order> {
  error: boolean;
  loaded: boolean;
  loading: boolean;
  selectedOrderId: string | null;
}

export function selectOrderId(order: Order): string {
  return order._id;
}

export function sortByName(order: Order, orderCompare: Order): number {
  return order._id.localeCompare(orderCompare._id);
}

export const adapter: EntityAdapter<Order> = createEntityAdapter<Order>({
  selectId: selectOrderId,
  sortComparer: sortByName
});

export const initialState: State = adapter.getInitialState({
  error: false,
  loaded: false,
  loading: false,
  selectedOrderId: null
});

export function reducer(state = initialState, action: OrdersActions): State {
  switch (action.type) {
    case OrdersActionTypes.LoadOrders: {
      return {
        ...state,
        loading: true
      };
    }

    case OrdersActionTypes.LoadOrdersSuccess: {
      const orders = action.payload.orders;
      const current = {
        ...state,
        error: false,
        loaded: true,
        loading: false
      };
      return adapter.addAll(orders, current);
    }

    case OrdersActionTypes.LoadOrderSuccess: {
      const order = action.payload.order;
      const current = {
        ...state
      };
      return adapter.upsertOne(order, current);
    }

    case OrdersActionTypes.LoadOrdersFail: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }

    case OrdersActionTypes.AddOrder:
    case OrdersActionTypes.UpdateOrder: {
      return {
        ...state,
        loading: true
      };
    }

    case OrdersActionTypes.AddOrderFail:
    case OrdersActionTypes.UpdateOrderFail:
    case OrdersActionTypes.DeleteOrderFail: {
      const error = action.payload.error;
      return {
        ...state,
        error,
        loading: false,
        loaded: false
      };
    }

    case OrdersActionTypes.AddOrderSuccess: {
      const order = action.payload.order;
      const current = {
        ...state,
        error: false,
        loading: false,
        loaded: true
      };
      return adapter.addOne(order, current);
    }

    case OrdersActionTypes.UpdateOrderSuccess: {
      const order = action.payload.order;
      const current = {
        ...state,
        error: false,
        loading: false,
        loaded: true
      };
      return adapter.updateOne({ id: order._id, changes: order }, current);
    }

    case OrdersActionTypes.DeleteOrderSuccess: {
      const id = action.payload.id;
      return adapter.removeOne(action.payload.id, state);
    }

    case OrdersActionTypes.ResetOrders: {
      return {
        ...state,
        error: false
      };
    }
  }

  return state;
}

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

// select the array of Order ids
export const selectOrdersIds = selectIds;

// select the dictionary of Order entities
export const selectOrdersEntities = selectEntities;

// select the array of Orders
export const selectOrdersAll = selectAll;

// select the total Order count
export const selectOrdersTotal = selectTotal;

export const getSelectedOrderId = (state: State) => state.selectedOrderId;
export const selectOrdersLoaded = (state: State) => state.loaded;
export const selectOrdersError = (state: State) => state.error;
export const selectOrdersLoading = (state: State) => state.loading;
