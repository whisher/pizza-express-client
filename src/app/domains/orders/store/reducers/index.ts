import { ActionReducerMap } from '@ngrx/store';

import * as fromOrders from './orders.reducer';

export interface OrdersState {
  orders: fromOrders.State;
}

export const reducers: ActionReducerMap<OrdersState> = {
  orders: fromOrders.reducer
};
