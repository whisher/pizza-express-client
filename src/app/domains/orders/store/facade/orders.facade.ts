import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Observable } from 'rxjs';

import { Order } from '../../models';
import { OrdersState } from '../reducers';
import {
  selectOrderById,
  selectOrdersError,
  selectOrdersIds,
  selectOrdersLoaded,
  selectOrdersLoading,
  selectOrdersAll,
  selectOrdersEntities
} from '../selectors';

import {
  AddOrder,
  DeleteOrder,
  LoadOrder,
  ResetOrders,
  UpdateOrder
} from '../actions';

export type OrderUpdate = Update<Order>;

@Injectable()
export class OrdersFacade {
  constructor(private store: Store<OrdersState>) {}

  get orderes$(): Observable<Order[]> {
    return this.store.pipe(select(selectOrdersAll));
  }

  get error$(): Observable<any> {
    return this.store.pipe(select(selectOrdersError));
  }

  get entities$(): Observable<{ [id: string]: Order }> {
    return this.store.pipe(select(selectOrdersEntities));
  }

  get ids$(): Observable<string[] | number[]> {
    return this.store.pipe(select(selectOrdersIds));
  }

  get loaded$(): Observable<boolean> {
    return this.store.pipe(select(selectOrdersLoaded));
  }

  get loading$(): Observable<boolean> {
    return this.store.pipe(select(selectOrdersLoading));
  }

  get selected$(): Observable<any> {
    return this.store.pipe(select(selectOrderById));
  }

  add(order: Order) {
    this.store.dispatch(new AddOrder({ order }));
  }

  delete(id: any) {
    this.store.dispatch(new DeleteOrder({ id }));
  }

  load(id: any) {
    this.store.dispatch(new LoadOrder({ id }));
  }

  reset() {
    this.store.dispatch(new ResetOrders());
  }

  update(order: Order) {
    this.store.dispatch(new UpdateOrder({ order }));
  }
}
