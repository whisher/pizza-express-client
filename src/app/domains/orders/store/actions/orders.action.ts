import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Order } from '../../models';

export const enum OrdersActionTypes {
  LoadOrders = '[Orders] Load Orders',
  LoadOrdersSuccess = '[Orders] Load Orders Success',
  LoadOrdersFail = '[Orders] Load Orders Fail',
  LoadOrder = '[Orders] Load Order',
  LoadOrderSuccess = '[Orders] Load Order Success',
  LoadOrderFail = '[Orders] Load Order Fail',
  AddOrder = '[Orders] Add Order',
  AddOrderSuccess = '[Orders] Add Order Success',
  AddOrderFail = '[Orders] Add Order Fail',
  UpdateOrder = '[Orders] Update Order',
  UpdateOrderSuccess = '[Orders] Update Order Success',
  UpdateOrderFail = '[Orders] Update Order Fail',
  DeleteOrder = '[Orders] Delete Order',
  DeleteOrderSuccess = '[Orders] Delete Order Success',
  DeleteOrderFail = '[Orders] Delete Order Fail',
  ResetOrders = '[Orders] Reset Orders'
}

export class LoadOrders implements Action {
  readonly type = OrdersActionTypes.LoadOrders;
}

export class LoadOrdersFail implements Action {
  readonly type = OrdersActionTypes.LoadOrdersFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadOrdersSuccess implements Action {
  readonly type = OrdersActionTypes.LoadOrdersSuccess;

  constructor(readonly payload: { orders: Order[] }) {}
}

export class LoadOrder implements Action {
  readonly type = OrdersActionTypes.LoadOrder;
  constructor(readonly payload: { id: any }) {}
}

export class LoadOrderFail implements Action {
  readonly type = OrdersActionTypes.LoadOrderFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadOrderSuccess implements Action {
  readonly type = OrdersActionTypes.LoadOrderSuccess;

  constructor(readonly payload: { order: Order }) {}
}

export class AddOrder implements Action {
  readonly type = OrdersActionTypes.AddOrder;

  constructor(readonly payload: { order: Order }) {}
}

export class AddOrderFail implements Action {
  readonly type = OrdersActionTypes.AddOrderFail;

  constructor(readonly payload: { error: any }) {}
}

export class AddOrderSuccess implements Action {
  readonly type = OrdersActionTypes.AddOrderSuccess;

  constructor(readonly payload: { order: Order }) {}
}

export class UpdateOrder implements Action {
  readonly type = OrdersActionTypes.UpdateOrder;

  constructor(readonly payload: { order: Order }) {}
}

export class UpdateOrderFail implements Action {
  readonly type = OrdersActionTypes.UpdateOrderFail;

  constructor(readonly payload: { error: any }) {}
}

export class UpdateOrderSuccess implements Action {
  readonly type = OrdersActionTypes.UpdateOrderSuccess;

  constructor(readonly payload: { order: Order }) {}
}

export class DeleteOrder implements Action {
  readonly type = OrdersActionTypes.DeleteOrder;

  constructor(readonly payload: { id: any }) {}
}

export class DeleteOrderFail implements Action {
  readonly type = OrdersActionTypes.DeleteOrderFail;

  constructor(readonly payload: { error: any }) {}
}

export class DeleteOrderSuccess implements Action {
  readonly type = OrdersActionTypes.DeleteOrderSuccess;

  constructor(readonly payload: { id: any }) {}
}

export class ResetOrders implements Action {
  readonly type = OrdersActionTypes.ResetOrders;
}

export type OrdersActions =
  | LoadOrders
  | LoadOrdersFail
  | LoadOrdersSuccess
  | LoadOrder
  | LoadOrderFail
  | LoadOrderSuccess
  | AddOrder
  | AddOrderFail
  | AddOrderSuccess
  | UpdateOrder
  | UpdateOrderFail
  | UpdateOrderSuccess
  | DeleteOrder
  | DeleteOrderFail
  | DeleteOrderSuccess
  | ResetOrders;
