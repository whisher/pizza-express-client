import { OrdersGuard } from './orders.guard';

export const guards: any[] = [OrdersGuard];

export * from './orders.guard';
