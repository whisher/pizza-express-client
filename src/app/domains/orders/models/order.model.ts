import { Dish } from '../../dishes';

export interface Order {
  _id?: string;
  userId: string;
  created?: number;
  dishes: Dish[];
  status: string;
}
