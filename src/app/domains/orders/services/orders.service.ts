import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { httpErrorHandler } from '../../../shared/utils';
import { Order } from '../models';

@Injectable()
export class OrdersService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.orders;
  }

  add(data: Order): Observable<Order> {
    return this.http
      .post<Order>(this.apiUrl, data)
      .pipe(catchError(httpErrorHandler));
  }

  load(): Observable<Order[]> {
    return this.http
      .get<Order[]>(this.apiUrl)
      .pipe(catchError(httpErrorHandler));
  }

  findById(id: string): Observable<Order> {
    return this.http
      .get<Order>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }

  update(id: any, Order: Order): Observable<Order> {
    return this.http
      .put<Order>(`${this.apiUrl}/${id}`, Order)
      .pipe(catchError(httpErrorHandler));
  }

  delete(id: string): Observable<Order> {
    return this.http
      .delete<Order>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }
}
