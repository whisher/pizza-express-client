// Core
import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';

// Libs
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Store
import { reducers, effects } from './store';
import * as fromService from './services';
import * as fromGuards from './guards';

@NgModule({
  imports: [
    StoreModule.forFeature('orders', reducers),
    EffectsModule.forFeature(effects)
  ]
})
export class DomainOrdersModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainOrdersModule) {
    if (parentModule) {
      throw new Error(
        'DomainOrdersModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainOrdersModule,
      providers: [...fromGuards.guards, ...fromService.services]
    };
  }
}
