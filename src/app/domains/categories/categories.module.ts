// Core
import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';

// Libs
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Store
import { reducers, effects } from './store';
import * as fromService from './services';
import * as fromGuards from './guards';

@NgModule({
  imports: [
    StoreModule.forFeature('categories', reducers),
    EffectsModule.forFeature(effects)
  ]
})
export class DomainCategoriesModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainCategoriesModule) {
    if (parentModule) {
      throw new Error(
        'DomainCategoriesModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainCategoriesModule,
      providers: [...fromGuards.guards, ...fromService.services]
    };
  }
}
