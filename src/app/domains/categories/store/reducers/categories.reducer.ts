import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import {
  CategoriesActionTypes,
  CategoriesActions
} from '../actions/categories.action';
import { Category } from '../../models';

export interface State extends EntityState<Category> {
  error: boolean;
  loaded: boolean;
  loading: boolean;
  selectedCategoryId: string | null;
}

export function selectCategoryId(category: Category): string {
  return category._id;
}

export function sortByName(
  category: Category,
  categoryCompare: Category
): number {
  return category.name.localeCompare(categoryCompare.name);
}

export const adapter: EntityAdapter<Category> = createEntityAdapter<Category>({
  selectId: selectCategoryId,
  sortComparer: sortByName
});

export const initialState: State = adapter.getInitialState({
  error: false,
  loaded: false,
  loading: false,
  selectedCategoryId: null
});

export function reducer(
  state = initialState,
  action: CategoriesActions
): State {
  switch (action.type) {
    case CategoriesActionTypes.LoadCategories: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoriesActionTypes.LoadCategoriesSuccess: {
      const categories = action.payload.categories;
      const current = {
        ...state,
        loaded: true,
        loading: false
      };
      return adapter.addAll(categories, current);
    }

    case CategoriesActionTypes.LoadCategorySuccess: {
      const category = action.payload.category;
      const current = {
        ...state
      };
      return adapter.upsertOne(category, current);
    }

    case CategoriesActionTypes.LoadCategoriesFail: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }

    case CategoriesActionTypes.AddCategory:
    case CategoriesActionTypes.UpdateCategory: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoriesActionTypes.AddCategoryFail:
    case CategoriesActionTypes.UpdateCategoryFail:
    case CategoriesActionTypes.DeleteCategoryFail: {
      const error = action.payload.error;
      return {
        ...state,
        error,
        loading: false,
        loaded: false
      };
    }

    case CategoriesActionTypes.AddCategorySuccess: {
      const current = {
        ...state,
        loading: false,
        loaded: true
      };
      return adapter.addOne(action.payload.category, current);
    }

    case CategoriesActionTypes.UpdateCategorySuccess: {
      const category = action.payload.category;
      return adapter.updateOne({ id: category._id, changes: category }, state);
    }

    case CategoriesActionTypes.DeleteCategorySuccess: {
      const id = action.payload.id;
      return adapter.removeOne(action.payload.id, state);
    }

    case CategoriesActionTypes.ResetCategory: {
      return {
        ...state,
        error: false
      };
    }
  }

  return state;
}

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

// select the array of Category ids
export const selectCategoriesIds = selectIds;

// select the dictionary of Category entities
export const selectCategoriesEntities = selectEntities;

// select the array of Categories
export const selectCategoriesAll = selectAll;

// select the total Category count
export const selectCategoriesTotal = selectTotal;

export const getSelectedCategoryId = (state: State) => state.selectedCategoryId;
export const selectCategoriesLoaded = (state: State) => state.loaded;
export const selectCategoriesError = (state: State) => state.error;
export const selectCategoriesLoading = (state: State) => state.loading;
