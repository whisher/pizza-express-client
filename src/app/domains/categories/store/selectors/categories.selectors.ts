import { createSelector, createFeatureSelector } from '@ngrx/store';

import { CategoriesState } from '../reducers';
import * as fromCategory from '../reducers/categories.reducer';

export const selectCategoryStatus = createFeatureSelector<CategoriesState>(
  'categories'
);
export const selectCategoryState = createSelector(
  selectCategoryStatus,
  (state: CategoriesState) => {
    return state.categories;
  }
);

export const selectCategoriesIds = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesIds
);
export const selectCategoriesEntities = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesEntities
);

export const selectCategoriesAll = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesAll
);

export const selectCategoriesError = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesError
);

export const selectCategoriesLoaded = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesLoaded
);

export const selectCategoriesLoading = createSelector(
  selectCategoryState,
  fromCategory.selectCategoriesLoading
);
