import { Injectable } from '@angular/core';

import { ActionsSubject, select, Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { Category } from '../../models';
import { CategoriesState } from '../reducers';
import {
  selectCategoriesError,
  selectCategoriesLoaded,
  selectCategoriesLoading,
  selectCategoriesAll,
  selectCategoriesEntities
} from '../selectors';

import {
  CategoriesActionTypes,
  AddCategory,
  AddCategorySuccess,
  DeleteCategory,
  ResetCategory,
  UpdateCategory,
  UpdateCategorySuccess
} from '../actions';

export type CategoryUpdate = Update<Category>;

@Injectable()
export class CategoriesFacade {
  constructor(
    private actionsSubject: ActionsSubject,
    private store: Store<CategoriesState>
  ) {}

  get categories$(): Observable<Category[]> {
    return this.store.pipe(select(selectCategoriesAll));
  }

  get error$(): Observable<any> {
    return this.store.pipe(select(selectCategoriesError));
  }

  get entities$(): Observable<{ [id: string]: Category }> {
    return this.store.pipe(select(selectCategoriesEntities));
  }

  get loaded$(): Observable<boolean> {
    return this.store.pipe(select(selectCategoriesLoaded));
  }

  get loading$(): Observable<boolean> {
    return this.store.pipe(select(selectCategoriesLoading));
  }

  add(category: Category) {
    this.store.dispatch(new AddCategory({ category }));
  }

  addSuccess(): Observable<Category> {
    return this.actionsSubject.pipe(
      filter(
        action => action.type === CategoriesActionTypes.AddCategorySuccess
      ),
      map((data: AddCategorySuccess) => {
        return data.payload.category;
      })
    );
  }

  delete(id: any) {
    this.store.dispatch(new DeleteCategory({ id }));
  }

  reset() {
    this.store.dispatch(new ResetCategory());
  }

  update(category: Category) {
    this.store.dispatch(new UpdateCategory({ category }));
  }

  updateSuccess(): Observable<Category> {
    return this.actionsSubject.pipe(
      filter(
        action => action.type === CategoriesActionTypes.UpdateCategorySuccess
      ),
      map((data: UpdateCategorySuccess) => {
        return data.payload.category;
      })
    );
  }
}
