import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Category } from '../../models';

export const enum CategoriesActionTypes {
  LoadCategories = '[Categories] Load Categories',
  LoadCategoriesSuccess = '[Categories] Load Categories Success',
  LoadCategoriesFail = '[Categories] Load Categories Fail',
  LoadCategory = '[Categories] Load Category',
  LoadCategorySuccess = '[Categories] Load Category Success',
  LoadCategoryFail = '[Categories] Load Category Fail',
  AddCategory = '[Categories] Add Category',
  AddCategorySuccess = '[Categories] Add Category Success',
  AddCategoryFail = '[Categories] Add Category Fail',
  UpdateCategory = '[Categories] Update Category',
  UpdateCategorySuccess = '[Categories] Update Category Success',
  UpdateCategoryFail = '[Categories] Update Category Fail',
  DeleteCategory = '[Categories] Delete Category',
  DeleteCategorySuccess = '[Categories] Delete Category Success',
  DeleteCategoryFail = '[Categories] Delete Category Fail',
  ResetCategory = '[Categories] Reset Category'
}

export class LoadCategories implements Action {
  readonly type = CategoriesActionTypes.LoadCategories;
}

export class LoadCategoriesFail implements Action {
  readonly type = CategoriesActionTypes.LoadCategoriesFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadCategoriesSuccess implements Action {
  readonly type = CategoriesActionTypes.LoadCategoriesSuccess;

  constructor(readonly payload: { categories: Category[] }) {}
}

export class LoadCategory implements Action {
  readonly type = CategoriesActionTypes.LoadCategory;
  constructor(readonly payload: { id: any }) {}
}

export class LoadCategoryFail implements Action {
  readonly type = CategoriesActionTypes.LoadCategoryFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.LoadCategorySuccess;

  constructor(readonly payload: { category: Category }) {}
}

export class AddCategory implements Action {
  readonly type = CategoriesActionTypes.AddCategory;

  constructor(readonly payload: { category: Category }) {}
}

export class AddCategoryFail implements Action {
  readonly type = CategoriesActionTypes.AddCategoryFail;

  constructor(readonly payload: { error: any }) {}
}

export class AddCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.AddCategorySuccess;

  constructor(readonly payload: { category: Category }) {}
}

export class UpdateCategory implements Action {
  readonly type = CategoriesActionTypes.UpdateCategory;

  constructor(readonly payload: { category: Category }) {}
}

export class UpdateCategoryFail implements Action {
  readonly type = CategoriesActionTypes.UpdateCategoryFail;

  constructor(readonly payload: { error: any }) {}
}

export class UpdateCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.UpdateCategorySuccess;

  constructor(readonly payload: { category: Category }) {}
}

export class DeleteCategory implements Action {
  readonly type = CategoriesActionTypes.DeleteCategory;

  constructor(readonly payload: { id: any }) {}
}

export class DeleteCategoryFail implements Action {
  readonly type = CategoriesActionTypes.DeleteCategoryFail;

  constructor(readonly payload: { error: any }) {}
}

export class DeleteCategorySuccess implements Action {
  readonly type = CategoriesActionTypes.DeleteCategorySuccess;

  constructor(readonly payload: { id: any }) {}
}

export class ResetCategory implements Action {
  readonly type = CategoriesActionTypes.ResetCategory;
}

export type CategoriesActions =
  | LoadCategories
  | LoadCategoriesFail
  | LoadCategoriesSuccess
  | LoadCategory
  | LoadCategoryFail
  | LoadCategorySuccess
  | AddCategory
  | AddCategoryFail
  | AddCategorySuccess
  | UpdateCategory
  | UpdateCategoryFail
  | UpdateCategorySuccess
  | DeleteCategory
  | DeleteCategoryFail
  | DeleteCategorySuccess
  | ResetCategory;
