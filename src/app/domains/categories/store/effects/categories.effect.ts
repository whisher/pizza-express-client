import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Update } from '@ngrx/entity';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Category } from '../../models';
import { Go } from '../../../router';
import * as actions from '../actions';

import * as fromServices from '../../services';

@Injectable()
export class CategoriesEffects {
  @Effect()
  loadCategories$ = this.actions$.pipe(
    ofType(actions.CategoriesActionTypes.LoadCategories),
    switchMap(() => {
      return this.service.load().pipe(
        map(
          (categories: Category[]) =>
            new actions.LoadCategoriesSuccess({ categories })
        ),
        catchError(error => of(new actions.LoadCategoriesFail({ error })))
      );
    })
  );

  @Effect()
  loadCategory$ = this.actions$.pipe(
    ofType(actions.CategoriesActionTypes.LoadCategory),
    map((action: actions.LoadCategory) => action.payload.id),
    switchMap((id: any) => {
      return this.service.findById(id).pipe(
        map(category => new actions.LoadCategorySuccess({ category })),
        catchError(error => of(new actions.LoadCategoryFail({ error })))
      );
    })
  );
  @Effect()
  addCategory$ = this.actions$.pipe(
    ofType(actions.CategoriesActionTypes.AddCategory),
    map((action: actions.AddCategory) => action.payload.category),
    switchMap((_category: Category) => {
      return this.service.add(_category).pipe(
        map(
          (category: Category) => new actions.AddCategorySuccess({ category })
        ),
        catchError(error => of(new actions.AddCategoryFail({ error })))
      );
    })
  );

  @Effect()
  updateCategory$ = this.actions$.pipe(
    ofType(actions.CategoriesActionTypes.UpdateCategory),
    map((action: actions.UpdateCategory) => action.payload.category),
    switchMap((_category: Category) => {
      return this.service.update(_category._id, _category).pipe(
        map(category => new actions.UpdateCategorySuccess({ category })),
        catchError(error => of(new actions.UpdateCategoryFail({ error })))
      );
    })
  );

  @Effect()
  deleteCategory$ = this.actions$.pipe(
    ofType(actions.CategoriesActionTypes.DeleteCategory),
    map((action: actions.DeleteCategory) => action.payload.id),
    switchMap(_id => {
      return this.service.delete(_id).pipe(
        map((id: any) => new actions.DeleteCategorySuccess({ id })),
        catchError(error => of(new actions.DeleteCategoryFail({ error })))
      );
    })
  );
  constructor(
    private actions$: Actions,
    private service: fromServices.CategoriesService
  ) {}
}
