import { CategoriesGuard } from './categories.guard';

export const guards: any[] = [CategoriesGuard];

export * from './categories.guard';
