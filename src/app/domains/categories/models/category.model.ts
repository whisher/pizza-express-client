export interface Category {
  _id?: string;
  created?: number;
  name: string;
  updated?: number;
}
