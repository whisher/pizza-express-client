import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { httpErrorHandler } from '../../../shared/utils';
import { Category } from '../models';

@Injectable()
export class CategoriesService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.categories;
  }

  add(data: Category): Observable<Category> {
    return this.http
      .post<Category>(this.apiUrl, data)
      .pipe(catchError(httpErrorHandler));
  }

  load(): Observable<Category[]> {
    return this.http
      .get<Category[]>(this.apiUrl)
      .pipe(catchError(httpErrorHandler));
  }

  findById(id: string): Observable<Category> {
    return this.http
      .get<Category>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }

  update(id: any, Category: Category): Observable<Category> {
    return this.http
      .put<Category>(`${this.apiUrl}/${id}`, Category)
      .pipe(catchError(httpErrorHandler));
  }

  delete(id: string): Observable<Category> {
    return this.http
      .delete<Category>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }
}
