import { Injectable } from '@angular/core';

import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { Actions, Effect, ofType } from '@ngrx/effects';

import { Account } from '../../models';
import { AccountService } from '../../services';
import { AccountActionTypes, AccountFailure, AccountSuccess } from '../actions';

@Injectable()
export class AccountEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType(AccountActionTypes.AccountLoad),
    switchMap(() => {
      return this.service.account().pipe(
        map((account: Account) => new AccountSuccess({ account })),
        catchError(error => of(new AccountFailure({ error })))
      );
    })
  );

  constructor(private actions$: Actions, private service: AccountService) {}
}
