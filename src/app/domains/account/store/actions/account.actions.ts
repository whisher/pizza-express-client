import { Action } from '@ngrx/store';
import { Account } from '../../models';

export const enum AccountActionTypes {
  AccountLoad = '[Account] Page Account',
  AccountFailure = '[Account] Api Account Failure',
  AccountSuccess = '[Account] Api Account Success'
}

export class AccountLoad implements Action {
  readonly type = AccountActionTypes.AccountLoad;
  constructor() {}
}

export class AccountFailure implements Action {
  readonly type = AccountActionTypes.AccountFailure;
  constructor(readonly payload: { error: any }) {}
}

export class AccountSuccess implements Action {
  readonly type = AccountActionTypes.AccountSuccess;
  constructor(readonly payload: { account: Account }) {}
}

export type AccountActions = AccountLoad | AccountFailure | AccountSuccess;
