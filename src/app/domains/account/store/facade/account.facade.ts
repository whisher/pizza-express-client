import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { Account } from '../../models';
import { State } from '../reducers';
import { selectError, selectLoading, selectAccount } from '../selectors';

import { AccountLoad } from '../actions';

@Injectable(/*{
  providedIn: 'root'
}*/)
export class AccountFacade {
  error$ = this.store.pipe(select(selectError));
  loading$ = this.store.pipe(select(selectLoading));
  account$ = this.store.pipe(select(selectAccount));

  constructor(private store: Store<State>) {}

  load() {
    this.store.dispatch(new AccountLoad());
  }
}
