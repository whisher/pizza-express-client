import { createSelector, createFeatureSelector } from '@ngrx/store';

import {
  AccountState,
  getError,
  getLoaded,
  getLoading,
  getAccount
} from '../reducers';

export const selectAccountState = createFeatureSelector<AccountState>(
  'account'
);

export const selectAccountStatus = createSelector(
  selectAccountState,
  (state: AccountState) => state.status
);
export const selectError = createSelector(
  selectAccountStatus,
  getError
);

export const selectLoaded = createSelector(
  selectAccountStatus,
  getLoaded
);

export const selectLoading = createSelector(
  selectAccountStatus,
  getLoading
);

export const selectAccount = createSelector(
  selectAccountStatus,
  getAccount
);
