import { ActionReducerMap } from '@ngrx/store';
import { reducer, State } from './account.reducer';

export interface AccountState {
  status: State;
}

export const accountReducer: ActionReducerMap<AccountState> = {
  status: reducer
};

export * from './account.reducer';
