import { Action } from '@ngrx/store';

import { Account } from '../../models';
import { AccountActions, AccountActionTypes } from '../actions';

export interface State {
  error: any;
  loaded: boolean;
  loading: boolean;
  account: Account | null;
}

export const initialState: State = {
  error: null,
  loaded: false,
  loading: false,
  account: null
};

export function reducer(state = initialState, action: AccountActions): State {
  switch (action.type) {
    case AccountActionTypes.AccountLoad: {
      return {
        ...state,
        error: null,
        loaded: false,
        loading: true
      };
    }

    case AccountActionTypes.AccountFailure: {
      return {
        ...state,
        error: action.payload,
        loaded: false,
        loading: false
      };
    }

    case AccountActionTypes.AccountSuccess: {
      return {
        ...state,
        account: action.payload.account,
        error: null,
        loaded: true,
        loading: false
      };
    }

    default: {
      return state;
    }
  }
}

export const getError = (state: State) => state.error;
export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getAccount = (state: State) => state.account;
