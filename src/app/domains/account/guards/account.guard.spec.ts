import { TestBed, async, inject } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { Account } from '../models';
import { AccountState, reducer, AccountSuccess } from '../store';

import { AccountGuard } from './account.guard';

describe('AccountGuard', () => {
  let store: Store<AccountState>;
  const account = {
    _id: 'abc',
    avatar: 'img',
    display_name: 'whisher',
    email: 'tu@tu.tu',
    firstname: 'yas',
    lastname: 'nia',
    last_login: 'ier',
    role: 'admin'
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({}),
        StoreModule.forFeature('account', combineReducers(reducer))
      ],
      providers: [Store, AccountGuard]
    });
    store = TestBed.get(Store);
  });

  it('should ...', inject([AccountGuard], (guard: AccountGuard) => {
    const action = new AccountSuccess({ account });
    console.log('guard account', guard);
    expect(guard).toBeTruthy();
  }));
});
