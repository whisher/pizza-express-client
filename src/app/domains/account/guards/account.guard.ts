// Core
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

// Rxjs
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';

// Libs
import { Store } from '@ngrx/store';

// Store
import { AccountLoad, AccountState, selectLoaded } from '../store';

@Injectable()
export class AccountGuard implements CanActivate {
  constructor(private store: Store<AccountState>) {}

  canActivate(): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(selectLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new AccountLoad());
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}
