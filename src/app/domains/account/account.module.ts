import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromGuards from './guards';
import * as fromService from './services';
import { accountReducer } from './store/reducers';
import { AccountEffects } from './store/effects';

@NgModule({
  imports: [
    StoreModule.forFeature('account', accountReducer),
    EffectsModule.forFeature([AccountEffects])
  ]
})
export class DomainAccountModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainAccountModule) {
    if (parentModule) {
      throw new Error(
        'DomainAccountModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainAccountModule,
      providers: [...fromGuards.guards, ...fromService.services]
    };
  }
}
