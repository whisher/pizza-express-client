export interface Account {
  _id: string;
  email: string;
  firstname: string;
  lastname: string;
  role?: string;
}
