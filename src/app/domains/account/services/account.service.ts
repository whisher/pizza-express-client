import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { Account } from '../models';
import { httpErrorHandler } from '../../../shared/utils';

@Injectable()
export class AccountService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.account;
  }

  account(): Observable<Account> {
    return this.http
      .get<Account>(this.apiUrl)
      .pipe(catchError(httpErrorHandler));
  }
}
