import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CONFIG, Config } from '../../../config/config.module';
import { AccountService } from './account.service';

describe('AccountService', () => {
  beforeEach(() => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AccountService,
        {
          provide: CONFIG,
          useClass: Config
        }
      ]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: AccountService = TestBed.get(AccountService);
    expect(service).toBeTruthy();
  });
});
