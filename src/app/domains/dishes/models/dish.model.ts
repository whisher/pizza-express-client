export interface Dish {
  _id?: string;
  categoryId?: string;
  categoryName?: string;
  created?: number;
  image?: string;
  ingredients: string[];
  name: string;
  qty: number;
  price: number;
  slug?: string;
  updated?: number;
}

export type dishesWithCategories = {
  [categoryName: string]: Dish[];
};
