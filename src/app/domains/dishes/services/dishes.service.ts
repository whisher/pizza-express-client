import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { httpErrorHandler } from '../../../shared/utils';
import { Dish } from '../models';

@Injectable()
export class DishesService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.dishes;
  }

  add(data: Dish): Observable<Dish> {
    return this.http
      .post<Dish>(this.apiUrl, data)
      .pipe(catchError(httpErrorHandler));
  }

  load(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.apiUrl).pipe(
      map((dishes: Dish[]) => {
        return dishes.map(dish => Object.assign({}, dish, { qty: 0 }));
      }),
      catchError(httpErrorHandler)
    );
  }

  findById(id: string): Observable<Dish> {
    return this.http
      .get<Dish>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }

  findByCategoryId(categoryId: string): Observable<Dish[]> {
    return this.http
      .get<Dish[]>(`${this.apiUrl}/category/${categoryId}`)
      .pipe(catchError(httpErrorHandler));
  }

  update(id: any, Dish: Dish): Observable<Dish> {
    return this.http
      .put<Dish>(`${this.apiUrl}/${id}`, Dish)
      .pipe(catchError(httpErrorHandler));
  }

  delete(id: string): Observable<Dish> {
    return this.http
      .delete<Dish>(`${this.apiUrl}/${id}`)
      .pipe(catchError(httpErrorHandler));
  }
}
