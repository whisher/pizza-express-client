import { ActionReducerMap } from '@ngrx/store';

import * as fromDishes from './dishes.reducer';

export interface DishesState {
  dishes: fromDishes.State;
}

export const reducers: ActionReducerMap<DishesState> = {
  dishes: fromDishes.reducer
};
