import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { DishesActionTypes, DishesActions } from '../actions/dishes.action';
import { Dish } from '../../models';

export interface State extends EntityState<Dish> {
  error: boolean;
  loaded: boolean;
  loading: boolean;
  selectedDishId: string | null;
}

export function selectDishId(dish: Dish): string {
  return dish._id;
}

export function sortByName(dish: Dish, dishCompare: Dish): number {
  return dish.name.localeCompare(dishCompare.name);
}

export const adapter: EntityAdapter<Dish> = createEntityAdapter<Dish>({
  selectId: selectDishId,
  sortComparer: sortByName
});

export const initialState: State = adapter.getInitialState({
  error: false,
  loaded: false,
  loading: false,
  selectedDishId: null
});

export function reducer(state = initialState, action: DishesActions): State {
  switch (action.type) {
    case DishesActionTypes.LoadDishes: {
      return {
        ...state,
        loading: true
      };
    }

    case DishesActionTypes.LoadDishesSuccess: {
      const dishes = action.payload.dishes;
      const current = {
        ...state,
        error: false,
        loaded: true,
        loading: false
      };
      return adapter.addAll(dishes, current);
    }

    case DishesActionTypes.LoadDishSuccess: {
      const dish = action.payload.dish;
      const current = {
        ...state
      };
      return adapter.upsertOne(dish, current);
    }

    case DishesActionTypes.LoadDishesFail: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }

    case DishesActionTypes.AddDish:
    case DishesActionTypes.UpdateDish: {
      return {
        ...state,
        loading: true
      };
    }

    case DishesActionTypes.AddDishFail:
    case DishesActionTypes.UpdateDishFail:
    case DishesActionTypes.DeleteDishFail: {
      const error = action.payload.error;
      return {
        ...state,
        error,
        loading: false,
        loaded: false
      };
    }

    case DishesActionTypes.AddDishSuccess: {
      const current = {
        ...state,
        error: false,
        loading: false,
        loaded: true
      };
      return adapter.addOne(action.payload.dish, current);
    }

    case DishesActionTypes.UpdateDishSuccess: {
      const dish = action.payload.dish;
      const current = {
        ...state,
        error: false,
        loading: false,
        loaded: true
      };
      return adapter.updateOne({ id: dish._id, changes: dish }, current);
    }

    case DishesActionTypes.DeleteDishSuccess: {
      const id = action.payload.id;
      return adapter.removeOne(id, state);
    }

    case DishesActionTypes.ResetDish: {
      return {
        ...initialState
      };
    }
  }

  return state;
}

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

// select the array of Dish ids
export const selectDishesIds = selectIds;

// select the dictionary of Dish entities
export const selectDishesEntities = selectEntities;

// select the array of Dishes
export const selectDishesAll = selectAll;

// select the total Dish count
export const selectDishesTotal = selectTotal;

export const getSelectedDishId = (state: State) => state.selectedDishId;
export const selectDishesLoaded = (state: State) => state.loaded;
export const selectDishesError = (state: State) => state.error;
export const selectDishesLoading = (state: State) => state.loading;
