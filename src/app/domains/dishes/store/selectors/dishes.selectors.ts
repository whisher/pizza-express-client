import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as fromRouter from '../../../router';
import { DishesState } from '../reducers';
import * as fromDish from '../reducers/dishes.reducer';
import * as fromCategories from '../../../categories/store/selectors';

export const selectDishStatus = createFeatureSelector<DishesState>('dishes');

export const selectDishState = createSelector(
  selectDishStatus,
  (state: DishesState) => {
    return state.dishes;
  }
);

export const selectDishesIds = createSelector(
  selectDishState,
  fromDish.selectDishesIds
);

export const selectDishesEntities = createSelector(
  selectDishState,
  fromDish.selectDishesEntities
);

export const selectDishById = createSelector(
  selectDishesEntities,
  fromRouter.selectRouterState,
  (entities, router) => {
    return router.state && entities[router.state.params.id];
  }
);

export const selectDishesAll = createSelector(
  selectDishState,
  fromDish.selectDishesAll
);

export const selectDishesWithCategoryName = createSelector(
  fromCategories.selectCategoriesEntities,
  selectDishesAll,
  (categories, dishes) => {
    if (dishes.length > 0) {
      const dishesWithName = dishes.map(dish => {
        dish.categoryName = categories[dish.categoryId].name;
        return dish;
      });
      return dishesWithName;
    }
    return [];
  }
);

export const selectDishesWithCategories = createSelector(
  fromCategories.selectCategoriesEntities,
  selectDishesAll,
  (categories, dishes) => {
    if (dishes.length > 0) {
      const dishesCategories = {};
      dishes.forEach(dish => {
        let categoryName = categories[dish.categoryId].name;
        if (!dishesCategories[categoryName]) {
          dishesCategories[categoryName] = [];
        }
        dishesCategories[categoryName].push(dish);
      });
      return dishesCategories;
    }
    return [];
  }
);

export const selectDishesError = createSelector(
  selectDishState,
  fromDish.selectDishesError
);

export const selectDishesLoaded = createSelector(
  selectDishState,
  fromDish.selectDishesLoaded
);

export const selectDishesLoading = createSelector(
  selectDishState,
  fromDish.selectDishesLoading
);
