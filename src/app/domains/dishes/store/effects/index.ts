import { DishesEffects } from './dishes.effect';

export const effects: any[] = [DishesEffects];

export * from './dishes.effect';
