import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Update } from '@ngrx/entity';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Dish } from '../../models';
import { Go } from '../../../router';
import * as actions from '../actions';

import * as fromServices from '../../services';

@Injectable()
export class DishesEffects {
  @Effect()
  loadDishes$ = this.actions$.pipe(
    ofType(actions.DishesActionTypes.LoadDishes),
    switchMap(() => {
      return this.service.load().pipe(
        map((dishes: Dish[]) => new actions.LoadDishesSuccess({ dishes })),
        catchError(error => of(new actions.LoadDishesFail({ error })))
      );
    })
  );

  @Effect()
  loadDish$ = this.actions$.pipe(
    ofType(actions.DishesActionTypes.LoadDish),
    map((action: actions.LoadDish) => {
      return new Go({ path: [`/admin/dishes/edit/${action.payload.id}`] });
    })
  );

  @Effect()
  addDish$ = this.actions$.pipe(
    ofType(actions.DishesActionTypes.AddDish),
    map((action: actions.AddDish) => action.payload.dish),
    switchMap((_dish: Dish) => {
      return this.service.add(_dish).pipe(
        map((dish: Dish) => new actions.AddDishSuccess({ dish })),
        catchError(error => of(new actions.AddDishFail({ error })))
      );
    })
  );

  @Effect()
  addOrUpdateDishSuccess$ = this.actions$.pipe(
    ofType(
      actions.DishesActionTypes.AddDishSuccess,
      actions.DishesActionTypes.UpdateDishSuccess
    ),
    map(() => new Go({ path: ['/admin/dishes'] }))
  );

  @Effect()
  updateDish$ = this.actions$.pipe(
    ofType(actions.DishesActionTypes.UpdateDish),
    map((action: actions.UpdateDish) => action.payload.dish),
    switchMap((_dish: Dish) => {
      return this.service.update(_dish._id, _dish).pipe(
        map((dish: Dish) => new actions.UpdateDishSuccess({ dish })),
        catchError(error => of(new actions.UpdateDishFail({ error })))
      );
    })
  );

  @Effect()
  deleteDish$ = this.actions$.pipe(
    ofType(actions.DishesActionTypes.DeleteDish),
    map((action: actions.DeleteDish) => action.payload.id),
    switchMap(_id => {
      return this.service.delete(_id).pipe(
        map((id: any) => new actions.DeleteDishSuccess({ id })),
        catchError(error => of(new actions.DeleteDishFail({ error })))
      );
    })
  );
  constructor(
    private actions$: Actions,
    private service: fromServices.DishesService
  ) {}
}
