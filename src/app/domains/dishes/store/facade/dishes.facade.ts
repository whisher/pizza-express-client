import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Observable } from 'rxjs';

import { Dish } from '../../models';
import { DishesState } from '../reducers';
import {
  selectDishById,
  selectDishesError,
  selectDishesIds,
  selectDishesLoaded,
  selectDishesLoading,
  selectDishesAll,
  selectDishesWithCategories,
  selectDishesWithCategoryName,
  selectDishesEntities
} from '../selectors';

import {
  AddDish,
  DeleteDish,
  LoadDish,
  ResetDish,
  UpdateDish
} from '../actions';

export type DishUpdate = Update<Dish>;

@Injectable()
export class DishesFacade {
  constructor(private store: Store<DishesState>) {}

  get dishes$(): Observable<Dish[]> {
    return this.store.pipe(select(selectDishesAll));
  }

  get dishesWithCategories$(): Observable<any> {
    return this.store.pipe(select(selectDishesWithCategories));
  }

  get dishesWithCategoryName$(): Observable<any> {
    return this.store.pipe(select(selectDishesWithCategoryName));
  }

  get error$(): Observable<any> {
    return this.store.pipe(select(selectDishesError));
  }

  get entities$(): Observable<{ [id: string]: Dish }> {
    return this.store.pipe(select(selectDishesEntities));
  }

  get ids$(): Observable<string[] | number[]> {
    return this.store.pipe(select(selectDishesIds));
  }

  get loaded$(): Observable<boolean> {
    return this.store.pipe(select(selectDishesLoaded));
  }

  get loading$(): Observable<boolean> {
    return this.store.pipe(select(selectDishesLoading));
  }

  get selected$(): Observable<any> {
    return this.store.pipe(select(selectDishById));
  }

  add(dish: Dish) {
    this.store.dispatch(new AddDish({ dish }));
  }

  delete(id: any) {
    this.store.dispatch(new DeleteDish({ id }));
  }

  load(id: any) {
    this.store.dispatch(new LoadDish({ id }));
  }

  reset() {
    this.store.dispatch(new ResetDish());
  }

  update(dish: Dish) {
    this.store.dispatch(new UpdateDish({ dish }));
  }
}
