import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Dish } from '../../models';

export const enum DishesActionTypes {
  LoadDishes = '[Dishes] Load Dishes',
  LoadDishesSuccess = '[Dishes] Load Dishes Success',
  LoadDishesFail = '[Dishes] Load Dishes Fail',
  LoadDish = '[Dishes] Load Dish',
  LoadDishSuccess = '[Dishes] Load Dish Success',
  LoadDishFail = '[Dishes] Load Dish Fail',
  AddDish = '[Dishes] Add Dish',
  AddDishSuccess = '[Dishes] Add Dish Success',
  AddDishFail = '[Dishes] Add Dish Fail',
  UpdateDish = '[Dishes] Update Dish',
  UpdateDishSuccess = '[Dishes] Update Dish Success',
  UpdateDishFail = '[Dishes] Update Dish Fail',
  DeleteDish = '[Dishes] Delete Dish',
  DeleteDishSuccess = '[Dishes] Delete Dish Success',
  DeleteDishFail = '[Dishes] Delete Dish Fail',
  ResetDish = '[Dishes] Reset Dish'
}

export class LoadDishes implements Action {
  readonly type = DishesActionTypes.LoadDishes;
}

export class LoadDishesFail implements Action {
  readonly type = DishesActionTypes.LoadDishesFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadDishesSuccess implements Action {
  readonly type = DishesActionTypes.LoadDishesSuccess;

  constructor(readonly payload: { dishes: Dish[] }) {}
}

export class LoadDish implements Action {
  readonly type = DishesActionTypes.LoadDish;
  constructor(readonly payload: { id: any }) {}
}

export class LoadDishFail implements Action {
  readonly type = DishesActionTypes.LoadDishFail;

  constructor(readonly payload: { error: any }) {}
}

export class LoadDishSuccess implements Action {
  readonly type = DishesActionTypes.LoadDishSuccess;

  constructor(readonly payload: { dish: Dish }) {}
}

export class AddDish implements Action {
  readonly type = DishesActionTypes.AddDish;

  constructor(readonly payload: { dish: Dish }) {}
}

export class AddDishFail implements Action {
  readonly type = DishesActionTypes.AddDishFail;

  constructor(readonly payload: { error: any }) {}
}

export class AddDishSuccess implements Action {
  readonly type = DishesActionTypes.AddDishSuccess;

  constructor(readonly payload: { dish: Dish }) {}
}

export class UpdateDish implements Action {
  readonly type = DishesActionTypes.UpdateDish;

  constructor(readonly payload: { dish: Dish }) {}
}

export class UpdateDishFail implements Action {
  readonly type = DishesActionTypes.UpdateDishFail;

  constructor(readonly payload: { error: any }) {}
}

export class UpdateDishSuccess implements Action {
  readonly type = DishesActionTypes.UpdateDishSuccess;

  constructor(readonly payload: { dish: Dish }) {}
}

export class DeleteDish implements Action {
  readonly type = DishesActionTypes.DeleteDish;

  constructor(readonly payload: { id: any }) {}
}

export class DeleteDishFail implements Action {
  readonly type = DishesActionTypes.DeleteDishFail;

  constructor(readonly payload: { error: any }) {}
}

export class DeleteDishSuccess implements Action {
  readonly type = DishesActionTypes.DeleteDishSuccess;

  constructor(readonly payload: { id: any }) {}
}

export class ResetDish implements Action {
  readonly type = DishesActionTypes.ResetDish;
}

export type DishesActions =
  | LoadDishes
  | LoadDishesFail
  | LoadDishesSuccess
  | LoadDish
  | LoadDishFail
  | LoadDishSuccess
  | AddDish
  | AddDishFail
  | AddDishSuccess
  | UpdateDish
  | UpdateDishFail
  | UpdateDishSuccess
  | DeleteDish
  | DeleteDishFail
  | DeleteDishSuccess
  | ResetDish;
