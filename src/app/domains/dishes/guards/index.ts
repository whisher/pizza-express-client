import { DishesGuard } from './dishes.guard';

export const guards: any[] = [DishesGuard];

export * from './dishes.guard';
