// Core
import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';

// Libs
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Store
import { reducers, effects } from './store';
import * as fromService from './services';
import * as fromGuards from './guards';

@NgModule({
  imports: [
    StoreModule.forFeature('dishes', reducers),
    EffectsModule.forFeature(effects)
  ]
})
export class DomainDishesModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainDishesModule) {
    if (parentModule) {
      throw new Error(
        'DomainDishesModule is already loaded. Import only in AppModule'
      );
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DomainDishesModule,
      providers: [...fromGuards.guards, ...fromService.services]
    };
  }
}
