// Core
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Env
import { environment } from '../environments/environment';

// Libs
import { TranslateModule } from '@ngx-translate/core';

// i18n
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
registerLocaleData(localeIt);

// Modules
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { DomainsModule } from './domains/domains.module';

// Components
import { AppComponent } from './app.component';

// Services
import { I18nService } from './core/i18n';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    AppRoutingModule,
    CoreModule.forRoot(),
    DomainsModule.forRoot()
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (i18n: I18nService) => {
        return () => {
          i18n.init(
            environment.defaultLanguage,
            environment.supportedLanguages
          );
        };
      },
      deps: [I18nService],
      multi: true
    },
    {
      provide: LOCALE_ID,
      useFactory: (i18n: I18nService) => {
        return i18n.language;
      },
      deps: [I18nService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
