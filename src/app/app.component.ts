import { Component } from '@angular/core';
import { RouterExtService } from './core/router';
@Component({
  selector: 'iwdf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(routerExtService: RouterExtService) {
    routerExtService.register();
  }
}
