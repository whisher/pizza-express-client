import { AbstractControl } from '@angular/forms';

export function passwordMatchValidator(
  control: AbstractControl
): { invalidRePassword: boolean } | null {
  if (control.get('password').value !== control.get('repassword').value) {
    return { invalidRePassword: true };
  }
  return null;
}
