import { Account } from '../../../domains/account';
import { roles } from '../../../domains/users';

export class AuthorizationService {
  static isAdmin(account: Account | null): boolean {
    if (!account) {
      return false;
    }
    return account.role === 'admin';
  }
}
