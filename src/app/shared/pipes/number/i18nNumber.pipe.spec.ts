import { I18nNumberPipe } from './i18nNumber.pipe';
import { I18nService } from '../../../core/i18n/i18n.service';

let i18nServiceStub: Partial<I18nService>;
i18nServiceStub = {
  language: 'en-US'
};

describe('I18nNumberPipe', () => {
  let pipe: I18nNumberPipe;
  const n = 1890.85;
  beforeEach(() => {
    pipe = new I18nNumberPipe(i18nServiceStub as I18nService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should be an USA number', () => {
    const format = pipe.transform(n);
    expect(format).toBe('1,890.85');
  });

  it('should be a italian number', () => {
    i18nServiceStub.language = 'it-IT';
    const format = pipe.transform(n);
    expect(format).toBe('1.890,85');
  });
});
