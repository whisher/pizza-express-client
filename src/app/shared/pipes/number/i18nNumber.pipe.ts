import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { I18nService } from '../../../core/i18n/i18n.service';

@Pipe({
  name: 'i18nNumber',
  pure: false // required to update the value when currentLang is changed
})
export class I18nNumberPipe implements PipeTransform {
  private value: string | null;
  private lastNumber: any;
  private lastLang: string;

  constructor(private i18nService: I18nService) {}

  transform(value: any, digitsInfo: string = '1.0-2'): any {
    const currentLang = this.i18nService.language;

    // if we ask another time for the same number & locale, return the last value
    if (value === this.lastNumber && currentLang === this.lastLang) {
      return this.value;
    }

    this.value = new DecimalPipe(currentLang).transform(value, digitsInfo);
    this.lastNumber = value;
    this.lastLang = currentLang;

    return this.value;
  }
}
