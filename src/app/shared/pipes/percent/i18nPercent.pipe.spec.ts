import { I18nPercentPipe } from './i18nPercent.pipe';
import { I18nService } from '../../../core/i18n/i18n.service';

let i18nServiceStub: Partial<I18nService>;
i18nServiceStub = {
  language: 'en-US'
};

describe('I18nPercentPipe', () => {
  let pipe: I18nPercentPipe;
  const p = 1.3495;
  beforeEach(() => {
    pipe = new I18nPercentPipe(i18nServiceStub as I18nService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should be an USA percent', () => {
    const format = pipe.transform(p);
    expect(format).toBe('134.95%');
  });

  it('should be a italian percent', () => {
    i18nServiceStub.language = 'it-IT';
    const format = pipe.transform(p);
    expect(format).toBe('134,95%');
  });
});
