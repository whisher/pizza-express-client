import { I18nCurrencyPipe } from './i18nCurrency.pipe';
import { I18nService } from '../../../core/i18n/i18n.service';

let i18nServiceStub: Partial<I18nService>;
i18nServiceStub = {
  language: 'en-US'
};

describe('I18nCurrencyPipe', () => {
  let pipe: I18nCurrencyPipe;
  const c = 1890.85;
  beforeEach(() => {
    pipe = new I18nCurrencyPipe(i18nServiceStub as I18nService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should be an USA currency', () => {
    const format = pipe.transform(c);
    expect(format).toBe('$1,890.85');
  });

  it('should be a italian currency', () => {
    i18nServiceStub.language = 'it-IT';
    const format = pipe.transform(c);
    expect(format).toBe('1.890,85 USD');
  });
});
