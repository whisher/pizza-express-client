import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { I18nService } from '../../../core/i18n/i18n.service';

@Pipe({
  name: 'i18nCurrency',
  pure: false // required to update the value when currentLang is changed
})
export class I18nCurrencyPipe implements PipeTransform {
  private value: string | null;
  private lastNumber: any;
  private lastLang: string;

  constructor(private i18nService: I18nService) {}

  transform(
    value: any,
    currencyCode?: string,
    display: 'code' | 'symbol' | 'symbol-narrow' | string | boolean = 'symbol',
    digitsInfo?: string
  ): any {
    const currentLang = this.i18nService.language;

    // if we ask another time for the same number & locale, return the last value
    if (value === this.lastNumber && currentLang === this.lastLang) {
      return this.value;
    }

    this.value = new CurrencyPipe(currentLang).transform(
      value,
      currencyCode,
      display,
      digitsInfo
    );
    this.lastNumber = value;
    this.lastLang = currentLang;

    return this.value;
  }
}
