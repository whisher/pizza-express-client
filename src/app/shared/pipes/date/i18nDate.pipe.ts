import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { I18nService } from '../../../core/i18n/i18n.service';

@Pipe({
  name: 'i18nDate',
  pure: false // required to update the value when currentLang is changed
})
export class I18nDatePipe implements PipeTransform {
  private value: string | null;
  private lastDate: any;
  private lastLang: string;

  constructor(public i18nService: I18nService) {}

  transform(date: any, pattern: string = 'mediumDate'): any {
    const currentLang = this.i18nService.language;

    // if we ask another time for the same date & locale, return the last value
    if (date === this.lastDate && currentLang === this.lastLang) {
      return this.value;
    }

    this.value = new DatePipe(currentLang).transform(date, pattern);
    this.lastDate = date;
    this.lastLang = currentLang;

    return this.value;
  }
}
