import { I18nDatePipe } from './i18nDate.pipe';
import { I18nService } from '../../../core/i18n/i18n.service';

let i18nServiceStub: Partial<I18nService>;
i18nServiceStub = {
  language: 'en-US'
};

describe('I18nDatePipe', () => {
  let pipe: I18nDatePipe;
  let d: Date;
  beforeEach(() => {
    pipe = new I18nDatePipe(i18nServiceStub as I18nService);
    d = new Date('2018-12-13 12:00:00');
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should be an USA date', () => {
    const format = pipe.transform(d, 'EEEE, MMMM d, y, h:mm:ss');
    expect(format).toBe('Thursday, December 13, 2018, 12:00:00');
  });

  it('should be a italian date', () => {
    i18nServiceStub.language = 'it-IT';
    const format = pipe.transform(d, 'EEEE, MMMM d, y, h:mm:ss');
    expect(format).toBe('giovedì, dicembre 13, 2018, 12:00:00');
  });
});
