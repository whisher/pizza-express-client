import { NgModule } from '@angular/core';

import { I18nCurrencyPipe } from './currency/i18nCurrency.pipe';
import { I18nDatePipe } from './date/i18nDate.pipe';
import { I18nNumberPipe } from './number/i18nNumber.pipe';
import { I18nPercentPipe } from './percent/i18nPercent.pipe';

const declarations = [
  I18nCurrencyPipe,
  I18nDatePipe,
  I18nNumberPipe,
  I18nPercentPipe
];

@NgModule({
  declarations: [...declarations],
  exports: [...declarations],
  imports: []
})
export class SharedPipesModule {}
