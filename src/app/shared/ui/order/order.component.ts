import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Cart } from '../../../domains/cart';
import { Dish } from '../../../domains/dishes';
@Component({
  selector: 'iwdf-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class IwdfOrderComponent {
  @Input() cart: Cart;
  @Output() checkout = new EventEmitter<any>();
  @Output() remove = new EventEmitter<Dish>();
  removeDish(dish: Dish) {
    this.remove.emit(dish);
  }
}
