import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { IwdfOrderComponent } from './order.component';
import { OrderPipe } from './order.pipe';
const IWDF_ORDER = [IwdfOrderComponent, OrderPipe];

@NgModule({
  imports: [CommonModule, MatButtonModule, MatIconModule],
  declarations: IWDF_ORDER,
  exports: IWDF_ORDER
})
export class IwdfOrderModule {}
