import { Pipe, PipeTransform } from '@angular/core';

import { Dish } from '../../../domains/dishes';
@Pipe({
  name: 'order'
})
export class OrderPipe implements PipeTransform {
  transform(dishes: Dish[]) {
    const _dishes = dishes.map(item => Object.assign({}, item, { num: 0 }));
    const entities = {};
    _dishes.forEach(dish => {
      let id = dish._id;
      if (!entities[id]) {
        entities[id] = dish;
      }
      entities[id].num = ++entities[id].num;
    });
    return Object.keys(entities).map(id => entities[id]);
  }
}
