import { Component, Input } from '@angular/core';

@Component({
  selector: 'iwdf-button-spinner',
  template: `
    <ng-container *ngIf="loading; else elseTemplate">
      <mat-progress-spinner
        mode="indeterminate"
        [strokeWidth]="2"
        [diameter]="32 * size"
        color="accent"
      ></mat-progress-spinner>
    </ng-container>
    <ng-template #elseTemplate>
      <span>{{ value }}</span>
    </ng-template>
  `,
  styles: [
    `
      .mat-progress-spinner {
        display: inline-block;
        vertical-align: middle;
      }
    `
  ]
})
export class IwdfButtonSpinnerComponent {
  @Input() loading = false;
  @Input() size = 1;
  @Input() value: string;
}
