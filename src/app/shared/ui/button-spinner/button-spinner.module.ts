import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material';
import { IwdfButtonSpinnerComponent } from './button-spinner.component';

const IWDF_BUTTON_SPINNER = [IwdfButtonSpinnerComponent];

@NgModule({
  imports: [CommonModule, MatProgressSpinnerModule],
  declarations: IWDF_BUTTON_SPINNER,
  exports: IWDF_BUTTON_SPINNER
})
export class IwdfButtonSpinnerModule {}
