import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IwdfDialogComponent } from './dialog.component';

describe('IwdfDialogComponent', () => {
  let component: IwdfDialogComponent;
  let fixture: ComponentFixture<IwdfDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IwdfDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwdfDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
