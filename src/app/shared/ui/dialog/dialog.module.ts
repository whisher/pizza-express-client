import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatDialogModule } from '@angular/material';

import { IwdfDialogComponent } from './dialog.component';

const IWDF_DIALOG = [IwdfDialogComponent];

@NgModule({
  imports: [CommonModule, MatButtonModule, MatDialogModule],
  declarations: IWDF_DIALOG,
  exports: IWDF_DIALOG
})
export class IwdfDialogModule {}
