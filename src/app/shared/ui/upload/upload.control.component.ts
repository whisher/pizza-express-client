import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';

const UPLOAD_CONTROL_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => IwdfUploadControlComponent),
  multi: true
};

@Component({
  selector: 'iwdf-upload-control',
  template: `
    <iwdf-upload
      [imagePreview$]="imagePreview$"
      (uploaded)="onImageUpload($event)"
    ></iwdf-upload>
  `,
  providers: [UPLOAD_CONTROL_ACCESSOR]
})
export class IwdfUploadControlComponent implements ControlValueAccessor {
  @Input() imagePreview$: Observable<string>;
  value: string;

  private onTouch: Function;
  private onModelChange: Function;

  registerOnTouched(fn) {
    this.onTouch = fn;
  }

  registerOnChange(fn) {
    this.onModelChange = fn;
  }

  writeValue(value: string) {
    this.value = value;
  }

  onImageUpload(imageUrl: string) {
    this.value = imageUrl;
    this.onModelChange(this.value);
    this.onTouch();
  }
}
