import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CONFIG, Config } from '../../../core/config';
import { httpErrorHandler } from '../../../shared/utils';

@Injectable()
export class UploadService {
  apiUrl: string;
  constructor(private http: HttpClient, @Inject(CONFIG) config: Config) {
    this.apiUrl = config.api.upload;
  }

  upload(file, name): Observable<string> {
    const postData = new FormData();
    postData.append('upload', file, name);
    return this.http
      .post<string>(this.apiUrl, postData)
      .pipe(catchError(httpErrorHandler));
  }
}
