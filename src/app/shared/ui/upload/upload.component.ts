import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  OnDestroy
} from '@angular/core';

import { Observable, Subscription } from 'rxjs';

import { UploadService } from './upload.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  providers: [UploadService]
})
export class IwdfUploadComponent implements OnDestroy {
  @ViewChild('file', { static: true }) file: ElementRef;
  @Input() imagePreview$: Observable<ArrayBuffer | string>;
  @Output() uploaded = new EventEmitter<string>();
  loading = false;
  subscription: Subscription;
  constructor(private service: UploadService) {}
  onFile(event) {
    event.preventDefault();
    event.stopPropagation();
    this.file.nativeElement.click();
  }

  onImageUpload(event: Event) {
    const file = (<HTMLInputElement>event.target).files[0];
    const filename = file.name.split('.')[0];
    this.imagePreview$ = this.fileReaderObs(file);
    this.loading = true;
    this.subscription = this.service
      .upload(file, filename)
      .subscribe((data: string) => {
        this.loading = false;
        this.uploaded.emit(data);
      });
  }

  fileReaderObs(blob: Blob): Observable<string> {
    return Observable.create(obs => {
      if (!(blob instanceof Blob)) {
        obs.error(new Error('`blob` must be an instance of File or Blob.'));
        return;
      }

      const reader = new FileReader();

      reader.onerror = err => obs.error(err);
      reader.onabort = err => obs.error(err);
      reader.onload = () => obs.next(reader.result);
      reader.onloadend = () => obs.complete();

      return reader.readAsDataURL(blob);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
