import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IwdfUploadComponent } from './upload.component';

describe('IwdfUploadComponent', () => {
  let component: IwdfUploadComponent;
  let fixture: ComponentFixture<IwdfUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IwdfUploadComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwdfUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
