import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';

import { IwdfLoaderModule } from '../loader/loader.module';

import { IwdfUploadComponent } from './upload.component';
import { IwdfUploadControlComponent } from './upload.control.component';

const IWDF_UPLOAD = [IwdfUploadComponent, IwdfUploadControlComponent];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IwdfLoaderModule
  ],
  declarations: IWDF_UPLOAD,
  exports: IWDF_UPLOAD
})
export class IwdfUploadModule {}
