import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IwdfConfirmComponent } from './confirm.component';

describe('IwdfConfirmComponent', () => {
  let component: IwdfConfirmComponent;
  let fixture: ComponentFixture<IwdfConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IwdfConfirmComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwdfConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
