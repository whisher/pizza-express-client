import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatDialogModule } from '@angular/material';

import { ConfirmService, IwdfConfirmComponent } from './confirm.component';

import { IsPristineGuard } from './confirm.guard';

const IWDF_CONFIRM_COMPONENTS = [IwdfConfirmComponent];
const IWDF_CONFIRM_PROVIDERS = [ConfirmService, IsPristineGuard];

@NgModule({
  imports: [CommonModule, MatButtonModule, MatDialogModule],
  declarations: IWDF_CONFIRM_COMPONENTS,
  providers: IWDF_CONFIRM_PROVIDERS,
  exports: IWDF_CONFIRM_COMPONENTS,
  entryComponents: [IwdfConfirmComponent]
})
export class IwdfConfirmModule {}
