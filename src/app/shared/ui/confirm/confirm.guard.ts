import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

import { Observable, of } from 'rxjs';
import { ConfirmService, IsPristineAware } from './confirm.component';

@Injectable()
export class IsPristineGuard implements CanDeactivate<IsPristineAware> {
  constructor(private confirmService: ConfirmService) {}
  canDeactivate(component: IsPristineAware): Observable<boolean> {
    if (!component.isPristine()) {
      return this.confirmService.openDialog({
        title: 'Warning',
        message: 'Are you sure you want to leave?'
      });
    }
    return of(true);
  }
}
