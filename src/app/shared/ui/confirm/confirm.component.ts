import { Inject, Component, Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
/**
 * The guarded Component should implement this interface
 */
export interface IsPristineAware {
  isPristine(): boolean;
}

export interface DialogData {
  title: string;
  message: string;
}

@Injectable()
export class ConfirmService {
  constructor(private dialog: MatDialog) {}
  openDialog(data: DialogData) {
    const dialogRef = this.dialog.open(IwdfConfirmComponent, {
      width: '350px',
      data
    });
    return dialogRef.afterClosed();
  }
}

/**
 * The component displayed in the confirmation modal opened by the ConfirmService.
 */
@Component({
  selector: 'iwdf-confirm',
  template: `
    <h1 mat-dialog-title>{{ data.title }}</h1>
    <div mat-dialog-content>
      {{ data.message }}
    </div>
    <div mat-dialog-actions align="end">
      <button mat-raised-button color="accent" (click)="no()" cdkFocusInitial>
        No
      </button>
      <button mat-raised-button color="warn" (click)="yes()">
        Yes
      </button>
    </div>
  `
})
export class IwdfConfirmComponent {
  constructor(
    public dialogRef: MatDialogRef<IwdfConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  yes() {
    this.dialogRef.close(true);
  }

  no() {
    this.dialogRef.close(false);
  }
}
