import { Directive, HostListener, Attribute } from '@angular/core';

import { IwdfStepperComponent } from './stepper.component';

@Directive({
  selector: '[iwdfStepperNext]'
})
export class IwdfStepperNextDirective {
  @HostListener('click') onClick() {
    this.stepper.next(this.current);
  }
  constructor(
    @Attribute('current') public current: number,
    private stepper: IwdfStepperComponent
  ) {}
}
