import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IwdfStepperComponent } from './stepper.component';

describe('IwdfStepperComponent', () => {
  let component: IwdfStepperComponent;
  let fixture: ComponentFixture<IwdfStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IwdfStepperComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwdfStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
