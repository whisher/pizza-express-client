// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Material
import { MatButtonModule } from '@angular/material/button';

// Components
import { IwdfStepComponent } from './step/step.component';
import { IwdfStepperComponent } from './stepper.component';
import { IwdfStepperNextDirective } from './stepper-next.directive';

@NgModule({
  declarations: [
    IwdfStepComponent,
    IwdfStepperComponent,
    IwdfStepperNextDirective
  ],
  imports: [CommonModule, MatButtonModule],
  exports: [IwdfStepComponent, IwdfStepperComponent, IwdfStepperNextDirective]
})
export class IwdfStepperModule {}
