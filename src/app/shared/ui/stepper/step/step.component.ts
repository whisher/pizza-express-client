import { Component, Input } from '@angular/core';

@Component({
  selector: 'iwdf-step',
  template: `
    <div [hidden]="!active">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./step.component.scss']
})
export class IwdfStepComponent {
  @Input() active = false;
}
