import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IwdfStepComponent } from './step.component';

describe('IwdfStepComponent', () => {
  let component: IwdfStepComponent;
  let fixture: ComponentFixture<IwdfStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IwdfStepComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwdfStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
