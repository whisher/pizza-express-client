import {
  AfterContentInit,
  Component,
  ContentChildren,
  QueryList
} from '@angular/core';

import { IwdfStepComponent } from './step/step.component';

@Component({
  selector: 'iwdf-stepper',
  template: `
    <ul class="stepper">
      <ng-container *ngFor="let step of steps; let i = index; let last = last">
        <li (click)="selectStep(step)">
          <button mat-mini-fab color="primary" [disabled]="!step.active">
            {{ i + 1 }}
          </button>
        </li>

        <li class="line" *ngIf="!last"></li>
      </ng-container>
    </ul>
    <ng-content></ng-content>
  `,
  styleUrls: ['./stepper.component.scss']
})
export class IwdfStepperComponent implements AfterContentInit {
  @ContentChildren(IwdfStepComponent) steps: QueryList<IwdfStepComponent>;

  // contentChildren are set
  ngAfterContentInit() {
    // get all active steps
    const activeSteps = this.steps.filter(step => step.active);

    // if there is no active step set, activate the first
    if (activeSteps.length === 0) {
      this.selectStep(this.steps.first);
    }
  }

  selectStep(step: IwdfStepComponent) {
    // deactivate all steps
    this.steps.toArray().forEach(step => {
      step.active = false;
    });

    // activate the step the user has clicked on.
    step.active = true;
  }
  next(current) {
    ++current;
    const step = this.steps.find((_, i) => {
      return current === i;
    });
    this.selectStep(step);
  }
}
