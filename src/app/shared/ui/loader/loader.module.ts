import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material';
import { IwdfLoaderComponent } from './loader.component';

const IWDF_LOADER = [IwdfLoaderComponent];

@NgModule({
  imports: [CommonModule, MatProgressSpinnerModule],
  declarations: IWDF_LOADER,
  exports: IWDF_LOADER
})
export class IwdfLoaderModule {}
