import { Component, Input } from '@angular/core';

@Component({
  selector: 'iwdf-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class IwdfLoaderComponent {
  @Input() size = 1;
  @Input() stroke = 1;
}
