import { Component, Input } from '@angular/core';

@Component({
  selector: 'iwdf-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class IwdfCartComponent {
  @Input() total = 0;
}
