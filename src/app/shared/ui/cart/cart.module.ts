import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { IwdfCartComponent } from './cart.component';

const IWDF_CART = [IwdfCartComponent];

@NgModule({
  imports: [CommonModule, MatIconModule],
  declarations: IWDF_CART,
  exports: IWDF_CART
})
export class IwdfCartModule {}
