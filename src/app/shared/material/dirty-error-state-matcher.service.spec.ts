import { TestBed } from '@angular/core/testing';

import { DirtyErrorStateMatcherService } from './dirty-error-state-matcher.service';

describe('DirtyErrorStateMatcherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DirtyErrorStateMatcherService = TestBed.get(
      DirtyErrorStateMatcherService
    );
    expect(service).toBeTruthy();
  });
});
