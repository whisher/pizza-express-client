import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Logger } from '../logger/logger.service';

const log = new Logger('ErrorInterceptor');

/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next
      .handle(request)
      .pipe(catchError(error => this.errorHandler(error)));
  }

  // Customize the default error handler here if needed
  private errorHandler(event: HttpEvent<any>) {
    if (event instanceof HttpResponse) {
      log.error('HttpResponse error', event.body);
    }
    log.error('Request error', event);
    return throwError(event);
  }
}
