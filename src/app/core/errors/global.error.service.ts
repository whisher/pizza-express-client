import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Logger } from '../logger';

const log = new Logger('GlobalErrorService');

@Injectable()
export class GlobalErrorService implements ErrorHandler {
  handleError(error: Error | HttpErrorResponse) {
    if (error instanceof HttpErrorResponse) {
      log.error('Server Error', error);
    } else {
      log.error('Client Error', error);
    }
  }
}
