import { environment } from '../../../../environments/environment';

import { Cdn } from '../models';

const cdnEndpoint = environment.cdn;

export const cdn: Cdn = {
  url: `${cdnEndpoint}`
};
