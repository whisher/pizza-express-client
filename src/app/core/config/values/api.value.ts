import { environment } from '../../../../environments/environment';

import { Api } from '../models';

const apiEndpoint = environment.apiEndpoint;

export const api: Api = {
  account: `${apiEndpoint}/api/account`,
  auth: {
    login: `${apiEndpoint}/api/users/login`,
    signup: `${apiEndpoint}/api/users/signup`
  },
  categories: `${apiEndpoint}/api/categories`,
  companies: `${apiEndpoint}/api/companies`,
  dishes: `${apiEndpoint}/api/dishes`,
  orders: `${apiEndpoint}/api/orders`,
  places: `${apiEndpoint}/api/places`,
  upload: `${apiEndpoint}/api/upload`,
  users: `${apiEndpoint}/api/users`
};
