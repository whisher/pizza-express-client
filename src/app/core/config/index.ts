import { InjectionToken } from '@angular/core';

import { Api } from './models';
import { api } from './values';
import { Cdn } from './models';
import { cdn } from './values';

export class Config {
  readonly api: Api = api;
  readonly cdn: Cdn = cdn;
}

export const CONFIG = new InjectionToken<Config>('app.config');
