export interface Api {
  account: string;
  auth: {
    login: string;
    signup: string;
  };
  categories: string;
  companies: string;
  dishes: string;
  orders: string;
  places: string;
  upload: string;
  users: string;
}
