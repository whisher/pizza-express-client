import { Injectable } from '@angular/core';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { Logger } from '../logger';
import enUS from '../../../translations/en-US.json';
import itIT from '../../../translations/it-IT.json';

const log = new Logger('I18nService');

export function extract(s: string) {
  return s;
}

@Injectable()
export class I18nService {
  languageKey = 'language';
  defaultLanguage: string;
  supportedLanguages: string[];

  constructor(private translateService: TranslateService) {
    translateService.setTranslation('en-US', enUS);
    translateService.setTranslation('it-IT', itIT);
  }

  init(defaultLanguage: string, supportedLanguages: string[]) {
    this.defaultLanguage = defaultLanguage;
    this.supportedLanguages = supportedLanguages;
    this.language = '';
    this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setLanguageKey(event.lang);
    });
  }

  set language(language: string) {
    language = this.getCurrentLanguage(language);
    log.debug(`Language set to ${language}`);
    this.translateService.use(language);
  }

  get language(): string {
    return this.translateService.currentLang;
  }

  getCurrentLanguage(language: string): string {
    language =
      language ||
      this.getLanguageKey() ||
      this.translateService.getBrowserCultureLang();

    let isSupportedLanguage = this.supportedLanguages.includes(language);

    // If no exact match is found, search without the region
    if (language && !isSupportedLanguage) {
      language = language.split('-')[0];
      language =
        this.supportedLanguages.find(supportedLanguage =>
          supportedLanguage.startsWith(language)
        ) || '';
      isSupportedLanguage = Boolean(language);
    }

    // Fallback if language is not supported
    if (!isSupportedLanguage) {
      language = this.defaultLanguage;
    }
    return language;
  }

  private setLanguageKey(current: string): void {
    localStorage.setItem(this.languageKey, current);
  }

  private getLanguageKey(): string {
    return localStorage.getItem(this.languageKey);
  }
}
