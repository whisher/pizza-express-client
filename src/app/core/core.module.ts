// Core
import {
  ErrorHandler,
  NgModule,
  ModuleWithProviders,
  Optional,
  SkipSelf
} from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Services
import { GlobalErrorService } from './errors/global.error.service';
import { ErrorInterceptor } from './errors/error.interceptor';
import { CONFIG, Config } from './config/index';
import { I18nService } from './i18n';
import { RouterExtService } from './router/router.service';
import { UiStore } from './ui-store/ui.store';

@NgModule()
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: ErrorHandler, useClass: GlobalErrorService },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true
        },
        {
          provide: CONFIG,
          useClass: Config
        },
        I18nService,
        RouterExtService,
        UiStore
      ]
    };
  }
}
