import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { filter, pairwise } from 'rxjs/operators';

@Injectable()
export class RouterExtService {
  private _previousUrl: string = undefined;
  private _currentUrl: string = undefined;

  constructor(private router: Router) {}

  register() {
    this._currentUrl = this.router.url;
    const onNavigationEnd = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      pairwise()
    );
    onNavigationEnd.subscribe(event => {
      this._previousUrl = event[0]['url'];
      this._currentUrl = event[1]['url'];
    });
  }

  get currentUrl() {
    return this._currentUrl;
  }

  get previousUrl() {
    return this._previousUrl;
  }
}
