import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { CategoriesGuard } from '../../domains/categories';
import { DishesGuard } from '../../domains/dishes';

// Component
import { DefaultLayoutMainComponent } from './layout/containers';

export const ROUTES: Routes = [
  {
    path: '',
    component: DefaultLayoutMainComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      },
      {
        path: 'about',
        loadChildren: () =>
          import('./about/about.module').then(m => m.DefaultAboutModule)
      },
      {
        path: 'contactus',
        loadChildren: () =>
          import('./contactus/contactus.module').then(
            m => m.DefaultContactUsModule
          )
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./home/home.module').then(m => m.DefaultHomeModule)
      },
      {
        path: 'resto',
        canActivate: [CategoriesGuard, DishesGuard],
        loadChildren: () =>
          import('./resto/resto.module').then(m => m.DefaultRestoModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class DefaultRoutingModule {}
