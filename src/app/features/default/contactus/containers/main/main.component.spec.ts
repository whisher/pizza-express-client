import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultContactUsMainComponent } from './main.component';

describe('BlogAboutMainComponent', () => {
  let component: DefaultContactUsMainComponent;
  let fixture: ComponentFixture<DefaultContactUsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultContactUsMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultContactUsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
