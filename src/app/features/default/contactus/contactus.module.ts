import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { DefaultContactUsRoutingModule } from './contactus-routing.module';

// Components
import * as fromContainers from './containers';

@NgModule({
  imports: [CommonModule, DefaultContactUsRoutingModule],
  declarations: [...fromContainers.containers]
})
export class DefaultContactUsModule {}
