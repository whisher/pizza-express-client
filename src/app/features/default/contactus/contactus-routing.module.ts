import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultContactUsMainComponent } from './containers';

export const ROUTES: Routes = [
  {
    path: '',
    component: DefaultContactUsMainComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class DefaultContactUsRoutingModule {}
