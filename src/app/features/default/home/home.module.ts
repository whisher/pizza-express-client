import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { IwdfMaterialModule } from '../../../shared/material';
import { DefaultHomeRoutingModule } from './home-routing.module';

import * as fromComponents from './components';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    IwdfMaterialModule,
    DefaultHomeRoutingModule
  ],
  declarations: [...fromComponents.components, ...fromContainers.containers]
})
export class DefaultHomeModule {}
