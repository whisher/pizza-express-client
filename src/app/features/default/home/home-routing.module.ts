import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultHomeMainComponent } from './containers/main/main.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: DefaultHomeMainComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class DefaultHomeRoutingModule {}
