import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-home-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class DefaultHomeMainComponent {
  constructor() {}
}
