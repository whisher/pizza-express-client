import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultHomeMainComponent } from './main.component';

describe('DefaultHomeMainComponent', () => {
  let component: DefaultHomeMainComponent;
  let fixture: ComponentFixture<DefaultHomeMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultHomeMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultHomeMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
