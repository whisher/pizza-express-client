import { DefaultLayoutBrandComponent } from './brand/brand.component';
import { DefaultLayoutFooterComponent } from './footer/footer.component';
import { DefaultLayoutNavComponent } from './nav/nav.component';

export const components: any[] = [
  DefaultLayoutBrandComponent,
  DefaultLayoutFooterComponent,
  DefaultLayoutNavComponent
];
