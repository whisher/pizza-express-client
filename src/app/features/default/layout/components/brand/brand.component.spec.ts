import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultLayoutBrandComponent } from './brand.component';

describe('DefaultBrandComponent', () => {
  let component: DefaultLayoutBrandComponent;
  let fixture: ComponentFixture<DefaultLayoutBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultLayoutBrandComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultLayoutBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
