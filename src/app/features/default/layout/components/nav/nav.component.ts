import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Cart } from '../../../../../domains/cart';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-layout-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class DefaultLayoutNavComponent {
  @Input() cart: Cart;
}
