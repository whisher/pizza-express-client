import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-layout-footer',
  template: `
    <p>&#169;Pizza Express - 2019</p>
  `,
  styles: [
    `
      p {
        margin: 0;
        color: #fff;
      }
    `
  ]
})
export class DefaultLayoutFooterComponent {
  constructor() {}
}
