// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';

// Components
import * as fromComponents from './components';
import * as fromContainers from './containers';

// Modules
import { IwdfCartModule } from '../../../shared/ui';

//import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule
} from '@angular/material';

@NgModule({
  imports: [CommonModule, RouterModule, FlexLayoutModule, IwdfCartModule],
  declarations: [...fromComponents.components, ...fromContainers.containers]
})
export class DefaultLayoutModule {}
