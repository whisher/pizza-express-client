import { Component, ChangeDetectionStrategy } from '@angular/core';

import { CartFacade } from '../../../../../domains/cart';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class DefaultLayoutHeaderComponent {
  constructor(private cartFacade: CartFacade) {}
  get cart$() {
    return this.cartFacade.cart$;
  }
}
