import { DefaultLayoutHeaderComponent } from './header/header.component';
import { DefaultLayoutMainComponent } from './main/main.component';

export const containers: any[] = [
  DefaultLayoutHeaderComponent,
  DefaultLayoutMainComponent
];

export * from './main/main.component';
