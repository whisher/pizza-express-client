import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultLayoutMainComponent } from './main.component';

describe('DefaultLayoutMainComponent', () => {
  let component: DefaultLayoutMainComponent;
  let fixture: ComponentFixture<DefaultLayoutMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultLayoutMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultLayoutMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
