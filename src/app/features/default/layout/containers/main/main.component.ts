import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'shop-layout-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class DefaultLayoutMainComponent {}
