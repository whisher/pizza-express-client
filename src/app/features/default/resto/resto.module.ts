import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { DefaultRestoRoutingModule } from './resto-routing.module';
import { IwdfMaterialModule } from '../../../shared/material';
import { IwdfLoaderModule, IwdfOrderModule } from '../../../shared/ui';

// Components
import * as fromComponents from './components';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    DefaultRestoRoutingModule,
    IwdfLoaderModule,
    IwdfMaterialModule,
    IwdfOrderModule
  ],
  declarations: [...fromComponents.components, ...fromContainers.containers]
})
export class DefaultRestoModule {}
