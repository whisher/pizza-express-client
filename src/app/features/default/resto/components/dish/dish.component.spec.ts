// Core
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';

// Components
import { DefaultRestoDishComponent } from './dish.component';

describe('DefaultRestoDishComponent', () => {
  let component: DefaultRestoDishComponent;
  let debugElement: DebugElement;
  let fixture: ComponentFixture<DefaultRestoDishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [DefaultRestoDishComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultRestoDishComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
