import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Dish } from '../../../../../domains/dishes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-resto-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss']
})
export class DefaultRestoDishComponent {
  @Input() dish: Dish;
}
