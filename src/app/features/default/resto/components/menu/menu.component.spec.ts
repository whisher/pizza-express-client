// Core
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';

// Components
import { DefaultRestoMenuComponent } from './menu.component';

describe('DefaultRestoMenuComponent', () => {
  let component: DefaultRestoMenuComponent;
  let debugElement: DebugElement;
  let fixture: ComponentFixture<DefaultRestoMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [DefaultRestoMenuComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultRestoMenuComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
