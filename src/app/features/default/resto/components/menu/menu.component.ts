import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

import { Dish } from '../../../../../domains/dishes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-resto-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class DefaultRestoMenuComponent {
  @Input() dishes: boolean;

  @Output() added = new EventEmitter<Dish>();

  step = 0;

  onAddToCart(dish: Dish) {
    this.added.emit(dish);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
