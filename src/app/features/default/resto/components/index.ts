import { DefaultRestoDishComponent } from './dish/dish.component';
import { DefaultRestoMenuComponent } from './menu/menu.component';

export const components: any[] = [
  DefaultRestoDishComponent,
  DefaultRestoMenuComponent
];
