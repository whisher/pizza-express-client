import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  Dish,
  DishesFacade,
  dishesWithCategories
} from '../../../../../domains/dishes';
import { CartFacade, Cart } from '../../../../../domains/cart';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'default-resto-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class DefaultRestoMainComponent implements OnInit {
  get dishes$(): Observable<dishesWithCategories> {
    return this.dishesFacade.dishesWithCategories$;
  }

  get loaded$() {
    return this.dishesFacade.loaded$;
  }

  get cart$(): Observable<Cart> {
    return this.cartFacade.cart$;
  }

  constructor(
    private dishesFacade: DishesFacade,
    private cartFacade: CartFacade
  ) {}

  ngOnInit() {
    this.cartFacade.start();
  }

  onRemoveToCart(dish: Dish) {
    this.cartFacade.remove(dish);
  }

  onAddToCart(dish: Dish) {
    this.cartFacade.add(dish);
  }
}
