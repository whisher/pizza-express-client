import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultRestoMainComponent } from './main.component';

describe('DefaultRestoMainComponent', () => {
  let component: DefaultRestoMainComponent;
  let fixture: ComponentFixture<DefaultRestoMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultRestoMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultRestoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
