import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultRestoMainComponent } from './containers';

export const ROUTES: Routes = [
  {
    path: '',
    component: DefaultRestoMainComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class DefaultRestoRoutingModule {}
