import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultAboutMainComponent } from './containers';

export const ROUTES: Routes = [
  {
    path: '',
    component: DefaultAboutMainComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class DefaultAboutRoutingModule {}
