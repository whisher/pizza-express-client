import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { DefaultAboutRoutingModule } from './about-routing.module';

// Components
import * as fromContainers from './containers';

@NgModule({
  imports: [CommonModule, DefaultAboutRoutingModule],
  declarations: [...fromContainers.containers]
})
export class DefaultAboutModule {}
