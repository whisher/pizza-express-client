import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultAboutMainComponent } from './main.component';

describe('DefaultAboutMainComponent', () => {
  let component: DefaultAboutMainComponent;
  let fixture: ComponentFixture<DefaultAboutMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultAboutMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultAboutMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
