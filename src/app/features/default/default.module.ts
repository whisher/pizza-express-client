import { NgModule } from '@angular/core';

import { DefaultLayoutModule } from './layout/layout.module';
import { DefaultRoutingModule } from './default-routing.module';

@NgModule({
  imports: [DefaultLayoutModule, DefaultRoutingModule],
  declarations: []
})
export class DefaultModule {}
