import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dashboard-welcome',
  template: `
    <div class="welcome">
      <h1>WELCOME</h1>
      <a class="btn btn-light btn-block btn-lg" routerLink="/admin/users">
        Go to user
      </a>
    </div>
  `,
  styles: [
    `
      :host {
        display: block;
      }
      .welcome {
        display: flex;
      }
    `
  ]
})
export class AdminDashboardWelcomeComponent {}
