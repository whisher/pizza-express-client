import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminLayoutModule } from './layout/layout.module';

@NgModule({
  declarations: [],
  imports: [AdminRoutingModule, AdminLayoutModule]
})
export class AdminModule {}
