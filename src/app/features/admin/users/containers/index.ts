import { AdminUsersFormMainComponent } from './form/form.component';
import { AdminUsersGridMainComponent } from './grid/grid.component';

export const containers: any[] = [
  AdminUsersFormMainComponent,
  AdminUsersGridMainComponent
];

export * from './form/form.component';
export * from './grid/grid.component';
