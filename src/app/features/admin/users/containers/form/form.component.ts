import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';

import { User, UsersFacade, RolesService } from '../../../../../domains/users';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-users-form-page',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AdminUsersFormMainComponent implements OnInit {
  loading$ = this.usersFacade.loading$;
  error$ = this.usersFacade.error$;
  selected$ = this.usersFacade.selected$;
  roles: Array<string> = [];
  pristine = true;
  constructor(
    private usersFacade: UsersFacade,
    private rolesService: RolesService
  ) {}
  ngOnInit() {
    this.usersFacade.reset();
    this.roles = this.rolesService.getRoles();
  }
  onIsPristine(event) {
    this.pristine = event;
  }

  isPristine() {
    return this.pristine;
  }
  onSubmit(user: User) {
    this.pristine = true;
    user._id ? this.usersFacade.update(user) : this.usersFacade.add(user);
  }
}
