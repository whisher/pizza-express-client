// Core
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Components
import { AdminUsersFormMainComponent } from './form.component';

@Component({
  selector: 'admin-users-form',
  template: ''
})
class MockAdminUsersFormComponent {}

describe('UsersFormMainComponent', () => {
  let component: AdminUsersFormMainComponent;
  let fixture: ComponentFixture<AdminUsersFormMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminUsersFormMainComponent, MockAdminUsersFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsersFormMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
