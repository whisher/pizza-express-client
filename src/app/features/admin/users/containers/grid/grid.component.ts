import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material';

import { UsersFacade, User } from '../../../../../domains/users';
import { IwdfDialogComponent } from '../../../../../shared/ui/dialog/dialog.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-users-grid-main',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class AdminUsersGridMainComponent {
  users$ = this.facade.users$;
  loaded$ = this.facade.loaded$;
  constructor(private dialog: MatDialog, private facade: UsersFacade) {}

  onEdit($event: User) {
    this.facade.load($event._id);
  }

  onDelete($event: User) {
    this.openDialog($event);
  }

  openDialog(user: User) {
    const dialogRef = this.dialog.open(IwdfDialogComponent, {
      data: user.firstname + user.lastname
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.facade.delete(user._id);
      }
    });
  }
}
