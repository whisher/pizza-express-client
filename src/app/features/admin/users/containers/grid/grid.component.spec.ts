import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsersGridMainComponent } from './grid.component';

describe('AdminUsersGridMainComponent', () => {
  let component: AdminUsersGridMainComponent;
  let fixture: ComponentFixture<AdminUsersGridMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminUsersGridMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsersGridMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
