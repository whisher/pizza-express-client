import { AdminUsersFormComponent } from './form/form.component';
import { AdminUsersGridComponent } from './grid/grid.component';

export const components: any[] = [
  AdminUsersFormComponent,
  AdminUsersGridComponent
];
