import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild
} from '@angular/core';

import { MatSort, MatTableDataSource } from '@angular/material';

import { CONFIG, Config } from '../../../../../core/config';
import { User } from '../../../../../domains/users';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-users-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class AdminUsersGridComponent implements AfterViewInit {
  @Input() data: User[];
  @Output() edited = new EventEmitter<User>();
  @Output() deleted = new EventEmitter<User>();

  displayedColumns: string[] = [
    'avatar',
    'firstname',
    'lastname',
    'email',
    'mobile',
    'role',
    'actions'
  ];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: MatTableDataSource<User>;

  cdnUrl: string;
  PREFIX = 'thumb_480_';

  constructor(@Inject(CONFIG) config: Config) {
    this.cdnUrl = config.cdn.url;
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
  }

  onEdit(row: User) {
    this.edited.emit(row);
  }

  onDelete(row: User) {
    this.deleted.emit(row);
  }
}
