import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription, Observable, of } from 'rxjs';
import { take } from 'rxjs/operators';

import { CONFIG, Config } from '../../../../../core/config';
import { User } from '../../../../../domains/users';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-users-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AdminUsersFormComponent implements OnInit, OnDestroy {
  /* Input */
  @Input() error: boolean;
  @Input() loading: boolean;
  @Input() selected: User;
  @Input() roles: Array<string>;

  /* Output */
  @Output() submitted = new EventEmitter<User>();
  @Output() pristine = new EventEmitter<boolean>();

  /* Form */
  frm: FormGroup;
  hide = true;
  subscription: Subscription;
  imagePreview$: Observable<string>;
  PREFIX = 'thumb_480_';
  cdnUrl: string;

  get isDisabled() {
    return this.frm.invalid || this.loading;
  }

  constructor(private fb: FormBuilder, @Inject(CONFIG) config: Config) {
    this.cdnUrl = config.cdn.url;
  }

  ngOnInit() {
    this.createForm();
    this.subscription = this.frm.valueChanges.pipe(take(1)).subscribe(_ => {
      this.pristine.emit(false);
    });
    if (this.selected) {
      const {
        avatar,
        firstname,
        lastname,
        email,
        mobile,
        role
      } = this.selected;
      const data = { avatar, firstname, lastname, email, mobile, role };
      this.frm.patchValue(data);
      const thumb = avatar
        ? `${this.cdnUrl}/${this.PREFIX}${avatar}`
        : '/assets/images/no-available-200.png';
      this.imagePreview$ = of(thumb);
    }
  }

  createForm() {
    const group = {
      avatar: ['', [Validators.required]],
      firstname: ['', [Validators.required]],
      lastname: [null, [Validators.required]],
      email: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      password: ['', [Validators.required]],
      role: ['', [Validators.required]]
    };
    if (this.selected) {
      delete group.password;
    }
    this.frm = this.fb.group(group);
  }

  onSubmit() {
    if (this.frm.valid) {
      this.submitted.emit({
        _id: this.selected ? this.selected._id : undefined,
        ...this.frm.value
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
