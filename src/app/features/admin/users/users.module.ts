// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { AdminUsersRoutingModule } from './users-routing.module';
import {
  IwdfButtonSpinnerModule,
  IwdfConfirmModule,
  IwdfDialogModule,
  IwdfLoaderModule,
  IwdfUploadModule
} from '../../../shared/ui';
import { IwdfMaterialModule } from '../../../shared/material';

// Components
import * as fromComponents from './components';
import * as fromContainers from './containers';

// Entries
import { IwdfDialogComponent } from '../../../shared/ui/dialog/dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    AdminUsersRoutingModule,
    IwdfButtonSpinnerModule,
    IwdfConfirmModule,
    IwdfDialogModule,
    IwdfLoaderModule,
    IwdfUploadModule,
    IwdfMaterialModule
  ],
  declarations: [...fromComponents.components, ...fromContainers.containers],
  entryComponents: [IwdfDialogComponent]
})
export class AdminUsersModule {}
