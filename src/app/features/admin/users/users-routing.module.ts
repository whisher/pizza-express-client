import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsPristineGuard } from '../../../shared/ui/confirm/confirm.guard';

import {
  AdminUsersFormMainComponent,
  AdminUsersGridMainComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    component: AdminUsersGridMainComponent,
    pathMatch: 'full'
  },
  {
    path: 'create',
    canDeactivate: [IsPristineGuard],
    component: AdminUsersFormMainComponent
  },
  {
    path: 'edit/:id',
    canDeactivate: [IsPristineGuard],
    component: AdminUsersFormMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminUsersRoutingModule {}
