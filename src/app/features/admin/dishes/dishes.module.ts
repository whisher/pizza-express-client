// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { AdminDishesRoutingModule } from './dishes-routing.module';
import {
  IwdfButtonSpinnerModule,
  IwdfConfirmModule,
  IwdfDialogModule,
  IwdfLoaderModule,
  IwdfUploadModule
} from '../../../shared/ui';
import { IwdfMaterialModule } from '../../../shared/material';

// Components
import * as fromComponents from './components';
import * as fromContainers from './containers';

// Entries
import { AdminDishesCategoryMainComponent } from './containers';
import { IwdfDialogComponent } from '../../../shared/ui/dialog/dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    AdminDishesRoutingModule,
    IwdfButtonSpinnerModule,
    IwdfConfirmModule,
    IwdfDialogModule,
    IwdfLoaderModule,
    IwdfMaterialModule,
    IwdfUploadModule
  ],
  declarations: [...fromComponents.components, ...fromContainers.containers],
  entryComponents: [AdminDishesCategoryMainComponent, IwdfDialogComponent]
})
export class AdminDishesModule {}
