import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesFormComponent } from './form.component';

describe('AdminDishesFormComponent', () => {
  let component: AdminDishesFormComponent;
  let fixture: ComponentFixture<AdminDishesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
