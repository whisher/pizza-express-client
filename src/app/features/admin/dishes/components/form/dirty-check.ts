import { combineLatest, Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';

export function dirtyCheck<U>(source: Observable<U>) {
  return function<T>(valueChanges: Observable<T>): Observable<boolean> {
    const isDirty$ = combineLatest(source, valueChanges).pipe(
      debounceTime(300),
      map(([a, b]) => {
        console.log(a);
        console.log(b);
        return JSON.stringify(a) == JSON.stringify(b);
      }),
      startWith(false)
    );

    return isDirty$;
  };
}
