import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription, Observable, of } from 'rxjs';
import { take } from 'rxjs/operators';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';

import { CONFIG, Config } from '../../../../../core/config';
import { Category } from '../../../../../domains/categories';
import { Dish } from '../../../../../domains/dishes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AdminDishesFormComponent implements OnChanges, OnDestroy, OnInit {
  /* Input */
  @Input() error: boolean;
  @Input() loading: boolean;
  @Input() categories: Category[];
  @Input() lastCategoryAdded: Category;
  @Input() selected: Dish;

  /* Output */
  @Output() addCategory = new EventEmitter<void>();
  @Output() submitted = new EventEmitter<Dish>();
  @Output() pristine = new EventEmitter<boolean>();

  /* Form */
  frm: FormGroup;
  subscription: Subscription;
  imagePreview$: Observable<string>;
  PREFIX = 'thumb_480_';
  cdnUrl: string;

  /* Chips */
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(private fb: FormBuilder, @Inject(CONFIG) config: Config) {
    this.cdnUrl = config.cdn.url;
  }

  ngOnInit() {
    this.createForm();
    this.subscription = this.frm.valueChanges.pipe(take(1)).subscribe(_ => {
      this.pristine.emit(false);
    });
    if (this.selected) {
      const { name, categoryId, image, ingredients, price } = this.selected;
      const data = { name, categoryId, image, price };
      this.frm.patchValue(data);

      ingredients.forEach((chip, i) => {
        this.ingredients.push(this.fb.control(chip));
      });
      this.imagePreview$ = of(`${this.cdnUrl}/${this.PREFIX}${image}`);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.lastCategoryAdded && !changes.lastCategoryAdded.firstChange) {
      if (
        changes.lastCategoryAdded.previousValue !==
        changes.lastCategoryAdded.currentValue
      ) {
        this.frm.controls['categoryId'].patchValue(
          changes.lastCategoryAdded.currentValue._id
        );
      }
    }
  }

  get ingredients() {
    return this.frm.get('ingredients') as FormArray;
  }

  get isDisabled() {
    return this.frm.invalid || this.loading;
  }

  createForm() {
    this.frm = this.fb.group({
      name: ['', [Validators.required]],
      price: [null, [Validators.required]],
      categoryId: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ingredients: this.fb.array([], [Validators.required])
    });
  }

  onAddChip(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.ingredients.push(this.fb.control(value.trim()));
    }
    if (input) {
      input.value = '';
    }
  }

  onRemoveChip(index: number): void {
    if (index >= 0) {
      this.ingredients.removeAt(index);
    }
  }

  onAddCategory(event) {
    event.preventDefault();
    event.stopPropagation();
    this.addCategory.emit();
  }

  onSubmit() {
    if (this.frm.valid) {
      this.submitted.emit({
        _id: this.selected ? this.selected._id : undefined,
        ...this.frm.value
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
