import { AdminDishesCategoryFormComponent } from './category/form/form.component';
import { AdminDishesCategoryGridComponent } from './category/grid/grid.component';
import { AdminDishesFormComponent } from './form/form.component';
import { AdminDishesGridComponent } from './grid/grid.component';

export const components: any[] = [
  AdminDishesCategoryFormComponent,
  AdminDishesCategoryGridComponent,
  AdminDishesFormComponent,
  AdminDishesGridComponent
];

export {
  AdminDishesCategoryFormComponent
} from './category/form/form.component';
