import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesCategoryFormComponent } from './form.component';

describe('AdminDishesCategoryFormComponent', () => {
  let component: AdminDishesCategoryFormComponent;
  let fixture: ComponentFixture<AdminDishesCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesCategoryFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
