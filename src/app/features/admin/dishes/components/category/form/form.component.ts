import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Category } from '../../../../../../domains/categories';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-category-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AdminDishesCategoryFormComponent implements OnInit {
  @Input() error: boolean;
  @Input() loading: boolean;
  @Output() submitted = new EventEmitter<Category>();
  selected: Category;
  frm: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  get isDisabled() {
    return this.frm.invalid || this.loading;
  }

  createForm() {
    this.frm = this.fb.group({
      name: ['', [Validators.required]]
    });
  }

  onEdit(category: Category) {
    this.selected = category;
    this.frm.setValue({ name: category.name });
  }

  onSubmit() {
    if (this.frm.valid) {
      this.submitted.emit(this.selected || this.frm.value);
    }
  }
}
