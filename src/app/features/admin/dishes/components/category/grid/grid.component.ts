import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { MatTableDataSource } from '@angular/material';

import { Category } from '../../../../../../domains/categories';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-category-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class AdminDishesCategoryGridComponent {
  displayedColumns: string[] = ['name', 'actions'];
  @Input() dataSource = new MatTableDataSource<Category>(this.dataSource);
  @Output() edited = new EventEmitter<Category>();
  onEdit(row: Category) {
    this.edited.emit(row);
  }
}
