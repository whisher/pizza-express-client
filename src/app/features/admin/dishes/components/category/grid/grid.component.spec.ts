import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesCategoryGridComponent } from './grid.component';

describe('AdminDishesCategoryGridComponent', () => {
  let component: AdminDishesCategoryGridComponent;
  let fixture: ComponentFixture<AdminDishesCategoryGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesCategoryGridComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesCategoryGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
