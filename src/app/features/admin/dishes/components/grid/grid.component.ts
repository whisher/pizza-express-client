import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild
} from '@angular/core';

import { MatSort, MatTableDataSource } from '@angular/material';

import { CONFIG, Config } from '../../../../../core/config';
import { Dish } from '../../../../../domains/dishes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class AdminDishesGridComponent implements AfterViewInit {
  @Input() data: Dish[];
  @Output() edited = new EventEmitter<Dish>();
  @Output() deleted = new EventEmitter<Dish>();

  displayedColumns: string[] = [
    'image',
    'category',
    'name',
    'ingredients',
    'price',
    'actions'
  ];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: MatTableDataSource<Dish>;

  cdnUrl: string;
  PREFIX = 'thumb_480_';

  constructor(@Inject(CONFIG) config: Config) {
    this.cdnUrl = config.cdn.url;
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
  }

  onEdit(row: Dish) {
    this.edited.emit(row);
  }

  onDelete(row: Dish) {
    this.deleted.emit(row);
  }
}
