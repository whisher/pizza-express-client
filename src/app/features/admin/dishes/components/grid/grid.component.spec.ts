import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesGridComponent } from './grid.component';

describe('AdminDishesGridComponent', () => {
  let component: AdminDishesGridComponent;
  let fixture: ComponentFixture<AdminDishesGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesGridComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
