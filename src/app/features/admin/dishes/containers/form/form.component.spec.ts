// Core
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Components
import { AdminDishesFormMainComponent } from './form.component';

@Component({
  selector: 'admin-dishes-form',
  template: ''
})
class MockAdminDishesFormComponent {}

describe('AdminDishesFormMainComponent', () => {
  let component: AdminDishesFormMainComponent;
  let fixture: ComponentFixture<AdminDishesFormMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesFormMainComponent, MockAdminDishesFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesFormMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
