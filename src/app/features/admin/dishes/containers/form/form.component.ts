import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { MatDialog } from '@angular/material';

import { CategoriesFacade, Category } from '../../../../../domains/categories';
import { DishesFacade, Dish } from '../../../../../domains/dishes';
import { AdminDishesCategoryMainComponent } from '../category/category.component';
import { IsPristineAware } from '../../../../../shared/ui/confirm/confirm.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-form-main',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AdminDishesFormMainComponent implements IsPristineAware, OnInit {
  loadingCategory$ = this.categoryFacade.loading$;
  errorCategory$ = this.categoryFacade.error$;
  categories$ = this.categoryFacade.categories$;
  loading$ = this.dishFacade.loading$;
  error$ = this.dishFacade.error$;
  selected$ = this.dishFacade.selected$;
  lastCategoryAdded: Category;
  pristine = true;

  constructor(
    private ref: ChangeDetectorRef,
    private dialog: MatDialog,
    private categoryFacade: CategoriesFacade,
    private dishFacade: DishesFacade
  ) {}

  ngOnInit() {
    this.dishFacade.reset();
  }

  onIsPristine(event) {
    this.pristine = event;
  }

  isPristine() {
    return this.pristine;
  }

  onAddCategory() {
    const dialogRef = this.dialog.open(AdminDishesCategoryMainComponent, {
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.lastCategoryAdded = result;
      this.ref.markForCheck();
    });
  }

  onSubmit(dish: Dish) {
    this.pristine = true;
    dish._id ? this.dishFacade.update(dish) : this.dishFacade.add(dish);
  }
}
