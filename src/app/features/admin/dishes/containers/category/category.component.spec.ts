import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesCategoryMainComponent } from './category.component';

describe('AdminDishesCategoryMainComponent', () => {
  let component: AdminDishesCategoryMainComponent;
  let fixture: ComponentFixture<AdminDishesCategoryMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesCategoryMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesCategoryMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
