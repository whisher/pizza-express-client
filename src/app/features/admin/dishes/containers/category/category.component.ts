import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Subscription } from 'rxjs';

import { MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { CategoriesFacade, Category } from '../../../../../domains/categories';
import { AdminDishesCategoryFormComponent } from '../../components';

@Component({
  selector: 'admin-dishes-category-main',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class AdminDishesCategoryMainComponent implements OnDestroy, OnInit {
  @ViewChild('cmp', { static: true }) cmp: AdminDishesCategoryFormComponent;
  loading$ = this.facade.loading$;
  error$ = this.facade.error$;
  categories$ = this.facade.categories$;

  subscription: Subscription;

  lastAdded: Category;

  constructor(
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AdminDishesCategoryMainComponent>,
    private facade: CategoriesFacade
  ) {}

  ngOnInit() {
    this.facade.reset();
  }

  onEdit(category: Category) {
    this.cmp.onEdit(category);
  }

  onSubmit(category: Category) {
    if (category._id) {
      this.facade.update(category);
      this.subscription = this.facade
        .updateSuccess()
        .subscribe((data: Category) => {
          const msg = 'Update category';
          const category = data.name;
          this.snackBar.open(msg, category, {
            duration: 1000
          });

          this.lastAdded = data;
        });
    } else {
      this.facade.add(category);
      this.subscription = this.facade
        .addSuccess()
        .subscribe((data: Category) => {
          const msg = 'Add category';
          const category = data.name;
          this.snackBar.open(msg, category, {
            duration: 1000
          });

          this.lastAdded = data;
        });
    }
  }

  close() {
    this.dialogRef.close(this.lastAdded);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
