import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DishesFacade, Dish } from '../../../../../domains/dishes';
import { IwdfDialogComponent } from '../../../../../shared/ui/dialog/dialog.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dishes-grid-main',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class AdminDishesGridMainComponent {
  dishes$ = this.facade.dishesWithCategoryName$;
  loaded$ = this.facade.loaded$;
  constructor(private dialog: MatDialog, private facade: DishesFacade) {}

  onEdit($event: Dish) {
    this.facade.load($event._id);
  }

  onDelete($event: Dish) {
    this.openDialog($event);
  }

  openDialog(dish: Dish) {
    const dialogRef = this.dialog.open(IwdfDialogComponent, {
      data: dish.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.facade.delete(dish._id);
      }
    });
  }
}
