import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDishesGridMainComponent } from './grid.component';

describe('AdminDishesGridMainComponent', () => {
  let component: AdminDishesGridMainComponent;
  let fixture: ComponentFixture<AdminDishesGridMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminDishesGridMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDishesGridMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
