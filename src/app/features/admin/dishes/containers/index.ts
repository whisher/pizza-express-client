import { AdminDishesCategoryMainComponent } from './category/category.component';
import { AdminDishesFormMainComponent } from './form/form.component';
import { AdminDishesGridMainComponent } from './grid/grid.component';

export const containers: any[] = [
  AdminDishesCategoryMainComponent,
  AdminDishesFormMainComponent,
  AdminDishesGridMainComponent
];

export {
  AdminDishesCategoryMainComponent
} from './category/category.component';
export { AdminDishesFormMainComponent } from './form/form.component';
export { AdminDishesGridMainComponent } from './grid/grid.component';
