import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsPristineGuard } from '../../../shared/ui/confirm/confirm.guard';

import {
  AdminDishesFormMainComponent,
  AdminDishesGridMainComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    component: AdminDishesGridMainComponent,
    pathMatch: 'full'
  },
  {
    path: 'create',
    canDeactivate: [IsPristineGuard],
    component: AdminDishesFormMainComponent
  },
  {
    path: 'edit/:id',
    canDeactivate: [IsPristineGuard],
    component: AdminDishesFormMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDishesRoutingModule {}
