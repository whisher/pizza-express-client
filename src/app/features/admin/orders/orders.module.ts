import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { AdminOrdersRoutingModule } from './orders-routing.module';

// Components
import * as fromComponents from './components';
import * as fromContainers from './containers';

@NgModule({
  imports: [CommonModule, AdminOrdersRoutingModule],
  declarations: [...fromComponents.components, ...fromContainers.containers]
})
export class AdminOrdersModule {}
