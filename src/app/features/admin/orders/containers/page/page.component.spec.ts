// Core
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Components
import { AdminOrdersPageComponent } from './page.component';

@Component({
  selector: 'admin-orders-form',
  template: ''
})
class MockAdminOrdersFormComponent {}

describe('MainComponent', () => {
  let component: AdminOrdersPageComponent;
  let fixture: ComponentFixture<AdminOrdersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminOrdersPageComponent, MockAdminOrdersFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrdersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
