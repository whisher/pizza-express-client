import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-orders-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class AdminOrdersPageComponent {}
