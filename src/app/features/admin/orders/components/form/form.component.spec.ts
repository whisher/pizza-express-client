import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOrdersFormComponent } from './form.component';

describe('AdminOrdersFormComponent', () => {
  let component: AdminOrdersFormComponent;
  let fixture: ComponentFixture<AdminOrdersFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminOrdersFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrdersFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
