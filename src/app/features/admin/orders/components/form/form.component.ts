import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-orders-form',
  template: `
    <div class="welcome"><h1>Orders</h1></div>
  `
})
export class AdminOrdersFormComponent {}
