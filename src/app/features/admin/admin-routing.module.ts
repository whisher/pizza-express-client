// Core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { AccountGuard } from '../../domains/account';
import { AuthGuard } from '../../domains/auth';
import { CategoriesGuard } from '../../domains/categories';
import { DishesGuard } from '../../domains/dishes';
import { UsersGuard } from '../../domains/users';

// Component
import { AdminLayoutMainComponent } from './layout/containers';

export const routes: Routes = [
  {
    path: '',
    component: AdminLayoutMainComponent,
    canActivate: [AuthGuard, AccountGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./dashboard/dashboard.module').then(
            m => m.AdminDashboardModule
          )
      },
      {
        path: 'dishes',
        canActivate: [CategoriesGuard, DishesGuard],
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./dishes/dishes.module').then(m => m.AdminDishesModule)
      },
      {
        path: 'orders',
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./orders/orders.module').then(m => m.AdminOrdersModule)
      },
      {
        path: 'users',
        canActivate: [UsersGuard],
        canLoad: [AuthGuard],
        loadChildren: () =>
          import('./users/users.module').then(m => m.AdminUsersModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
