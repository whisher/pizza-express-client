import { AdminLayoutAccountComponent } from './account/account.component';
import { AdminLayoutBrandComponent } from './brand/brand.component';
import { AdminLayoutFooterComponent } from './footer/footer.component';
import { AdminLayoutNavComponent } from './nav/nav.component';
import { AdminLayoutNotificationsComponent } from './notifications/notifications.component';

export const components: any[] = [
  AdminLayoutAccountComponent,
  AdminLayoutBrandComponent,
  AdminLayoutFooterComponent,
  AdminLayoutNavComponent,
  AdminLayoutNotificationsComponent
];
