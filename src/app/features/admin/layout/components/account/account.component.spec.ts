import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLayoutAccountComponent } from './account.component';

describe('AdminLayoutAccountComponent', () => {
  let component: AdminLayoutAccountComponent;
  let fixture: ComponentFixture<AdminLayoutAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminLayoutAccountComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLayoutAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
