import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

import { Account } from '../../../../../domains/account';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-layout-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AdminLayoutAccountComponent implements OnInit {
  @Input() account: Account;
  @Output() logout = new EventEmitter<any>();
  current: string;
  ngOnInit() {
    this.current = this.account.firstname + ' ' + this.account.lastname;
  }
  onLogout() {
    this.logout.emit();
  }
}
