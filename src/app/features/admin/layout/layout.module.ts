// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { IwdfMaterialModule } from '../../../shared/material';

// Components
import * as fromContainers from './containers';
import * as fromComponents from './components';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    TranslateModule,
    IwdfMaterialModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components]
})
export class AdminLayoutModule {}
