// Core
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

// Components
import { AdminLayoutMainComponent } from './main.component';

@Component({
  selector: 'admin-layout-footer',
  template: ''
})
class MockAdminLayoutFooterComponent {}

@Component({
  selector: 'admin-layout-header',
  template: ''
})
class MockAdminLayoutHeaderComponent {}

describe('AdminLayoutMainComponent', () => {
  let component: AdminLayoutMainComponent;
  let el: DebugElement;
  let fixture: ComponentFixture<AdminLayoutMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AdminLayoutMainComponent,
        MockAdminLayoutFooterComponent,
        MockAdminLayoutHeaderComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLayoutMainComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a router outlet`, () => {
    const outlet = el.query(By.directive(RouterOutlet));
    expect(outlet).not.toBeNull();
  });
});
