import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  ViewChild
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MediaChange, MediaObserver } from '@angular/flex-layout';

import { MatDrawer } from '@angular/material';

import { AccountFacade } from '../../../../../domains/account';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-layout-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class AdminLayoutMainComponent implements OnDestroy {
  @ViewChild('drawer', { read: MatDrawer, static: true }) drawer: MatDrawer;
  opened$: Observable<boolean>;
  notifications = 15;
  account$ = this.facade.account$;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    media: MediaObserver,
    private facade: AccountFacade
  ) {
    this.opened$ = media.media$.pipe(
      map((change: MediaChange) => {
        if (change.mqAlias == 'xs') {
          return false;
        }
        return true;
      })
    );
    this.document.body.classList.remove('default-theme');
    document.body.classList.add('admin-theme');
  }

  onToggle() {
    this.drawer.toggle();
  }

  onLogout() {
    this.router.navigateByUrl('/auth/logout');
  }

  public ngOnDestroy(): void {
    this.document.body.classList.remove('admin-theme');
    document.body.classList.add('default-theme');
  }
}
