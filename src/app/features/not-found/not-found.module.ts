// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Libs
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { NotFoundRoutingModule } from './not-found-routing.module';

// Component
import { NotFoundPageComponent } from './page/page.component';

@NgModule({
  declarations: [NotFoundPageComponent],
  imports: [CommonModule, TranslateModule, NotFoundRoutingModule]
})
export class NotFoundModule {}
