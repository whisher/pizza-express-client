import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Credentials } from '../../../../domains/auth';

import { DirtyErrorStateMatcherService } from '../../../../shared/material';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AuthLoginFormComponent implements OnInit {
  @Input() error: boolean;
  @Input() loading: boolean;

  @Output() submitted = new EventEmitter<Credentials>();

  frm: FormGroup;

  matcher = new DirtyErrorStateMatcherService();

  hide = true;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  get isDisabled() {
    return this.frm.invalid || this.loading;
  }

  createForm() {
    this.frm = this.fb.group({
      email: [
        '',
        {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
        }
      ],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.frm.valid) {
      this.submitted.emit(this.frm.value);
    }
  }
}
