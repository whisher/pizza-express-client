import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { passwordMatchValidator } from '../../../../shared/utils';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-signup-form',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class AuthSignupFormComponent implements OnInit {
  @Input() error: boolean;
  @Input() loading: boolean;

  @Output() submitted = new EventEmitter<any>();

  isLinear = false;
  hide = true;
  formGroupOne: FormGroup;
  formGroupTwo: FormGroup;
  formGroupThree: FormGroup;

  get isDisabledOne() {
    return this.formGroupOne.invalid;
  }
  get isDisabledTwo() {
    return this.formGroupTwo.invalid;
  }
  get isDisabledThree() {
    return this.formGroupThree.invalid;
  }
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.createFormGroupOne();
    this.createFormGroupTwo();
    this.createFormGroupThree();
  }

  createFormGroupOne() {
    this.formGroupOne = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required]
    });
  }

  createFormGroupTwo() {
    this.formGroupTwo = this.fb.group({
      email: [
        '',
        {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
        }
      ],
      mobile: ['', Validators.required]
    });
  }

  createFormGroupThree() {
    this.formGroupThree = this.fb.group(
      {
        password: ['', Validators.required],
        repassword: ['', Validators.required]
      },
      { validator: passwordMatchValidator }
    );
  }

  onSubmit() {
    if (
      this.formGroupOne.valid &&
      this.formGroupTwo.valid &&
      this.formGroupThree.valid
    ) {
      const data = {
        ...this.formGroupOne.value,
        ...this.formGroupTwo.value,
        password: this.formGroupThree.value.password
      };
      console.log('data', data);
      this.submitted.emit(data);
    }
  }
}
