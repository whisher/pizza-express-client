import { AuthLoginFormComponent } from './login/login.component';
import { AuthSignupFormComponent } from './signup/signup.component';

export const components: any[] = [
  AuthLoginFormComponent,
  AuthSignupFormComponent
];
