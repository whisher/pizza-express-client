import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { AuthFacade } from '../../../../domains/auth';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class AuthLogoutPageComponent implements OnInit {
  constructor(private facade: AuthFacade) {}
  ngOnInit() {
    this.facade.logout();
  }
}
