import { AuthAccountComponent } from './account/account.component';
import { AuthLoginPageComponent } from './login/login.component';
import { AuthLogoutPageComponent } from './logout/logout.component';
import { AuthSignupPageComponent } from './signup/signup.component';

export const containers: any[] = [
  AuthAccountComponent,
  AuthLoginPageComponent,
  AuthLogoutPageComponent,
  AuthSignupPageComponent
];

export { AuthAccountComponent } from './account/account.component';
export { AuthLoginPageComponent } from './login/login.component';
export { AuthLogoutPageComponent } from './logout/logout.component';
export { AuthSignupPageComponent } from './signup/signup.component';
