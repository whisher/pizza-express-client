import { ChangeDetectionStrategy, Component } from '@angular/core';

import { AuthFacade } from '../../../../domains/auth';
import { User } from '../../../../domains/users';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-signup-page',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class AuthSignupPageComponent {
  error$ = this.facade.error$;
  loading$ = this.facade.loading$;
  constructor(private facade: AuthFacade) {}
  onSubmit($event: User) {
    this.facade.signup($event);
  }
}
