import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { Account, AccountFacade } from '../../../../domains/account';

@Component({
  selector: 'auth-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AuthAccountComponent implements OnDestroy, OnInit {
  subscription: Subscription;
  loading$ = this.facade.loading$;

  constructor(private router: Router, private facade: AccountFacade) {}

  ngOnInit() {
    this.subscription = this.facade.account$.subscribe((account: Account) => {
      if (!account) {
        this.router.navigateByUrl('/');
      } else if (account.role === 'user') {
        this.router.navigateByUrl('/');
      } else {
        this.router.navigateByUrl('/admin');
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
