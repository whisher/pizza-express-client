import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Credentials, AuthFacade } from '../../../../domains/auth';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AuthLoginPageComponent implements OnInit {
  error$ = this.facade.error$;
  loading$ = this.facade.loading$;
  constructor(private facade: AuthFacade) {}
  ngOnInit() {
    this.facade.logout();
  }
  onSubmit($event: Credentials) {
    this.facade.login($event);
  }
}
