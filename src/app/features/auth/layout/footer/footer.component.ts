import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'auth-layout-footer',
  template: `
    <footer fxLayout fxLayoutAlign="center">
      <p>&#169;Pizza express - 2019</p>
    </footer>
  `,
  styles: [
    `
      :host {
        display: block;
      }
      p {
        margin-bottom: 0;
        line-height: 2.5;
      }
    `
  ]
})
export class AuthLayoutFooterComponent {}
