// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Components
import { AuthLayoutFooterComponent } from './footer/footer.component';
import { AuthLayoutHeaderComponent } from './header/header.component';
import { AuthLayoutMainComponent } from './main/main.component';

@NgModule({
  declarations: [
    AuthLayoutFooterComponent,
    AuthLayoutHeaderComponent,
    AuthLayoutMainComponent
  ],
  imports: [CommonModule, RouterModule, FlexLayoutModule, TranslateModule]
})
export class AuthLayoutModule {}
