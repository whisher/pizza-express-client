// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

// Modules
import { AuthLayoutModule } from './layout/auth-layout.module';
import { AuthRoutingModule } from './auth-routing.module';
import { IwdfMaterialModule } from '../../shared/material';
import { IwdfButtonSpinnerModule, IwdfLoaderModule } from '../../shared/ui';

// Components
import * as fromContainers from './containers';
import * as fromComponents from './components';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    AuthLayoutModule,
    AuthRoutingModule,
    IwdfLoaderModule,
    IwdfMaterialModule,
    IwdfButtonSpinnerModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components]
})
export class AuthModule {}
