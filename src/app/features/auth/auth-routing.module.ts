// Core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { AuthLoginGuard } from '../../domains/auth';
import { AccountGuard } from '../../domains/account';

// Components
import { AuthLayoutMainComponent } from './layout/main/main.component';
import {
  AuthAccountComponent,
  AuthLoginPageComponent,
  AuthLogoutPageComponent,
  AuthSignupPageComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    component: AuthLayoutMainComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login' },
      {
        canActivate: [AuthLoginGuard],
        path: 'login',
        component: AuthLoginPageComponent
      },
      {
        canActivate: [AuthLoginGuard],
        path: 'signup',
        component: AuthSignupPageComponent
      }
    ]
  },
  {
    canActivate: [AccountGuard],
    path: 'account',
    component: AuthAccountComponent
  },
  { path: 'logout', component: AuthLogoutPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
