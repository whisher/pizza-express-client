import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  defaultLanguage: 'en-US',
  supportedLanguages: ['en-US', 'it-IT'],
  apiEndpoint: 'https://quiet-plateau-89508.herokuapp.com',
  cdn: 'https://quiet-plateau-89508.herokuapp.com/images'
};
