export interface Environment {
  production: boolean;
  defaultLanguage: string;
  supportedLanguages: string[];
  apiEndpoint: string;
  cdn: string;
}
