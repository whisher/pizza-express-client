# Blogial

# Getting started

1. Go to project folder and install dependencies:

```bash
npm install
```

2. Launch development server, and open `localhost:4200` in your browser:

```bash
npm start
```

# Project structure

```
dist/ compiled version
documentation/ project docs with compodoc
e2e/ end-to-end tests
src/ project source code
|- app/ app components
| |- config app configuration (api urls)
| |- core/ core module (singleton services and single-use components)
| |- domains stores, services, guards, models
| |- features/ features module (admin(dashboard,users,groups...), auth)
| |- shared/ shared module (common components ui, directives and pipes)
| |- utils general utilities
| |- app.component.* app root component (shell)
| |- app.module.ts app root module definition
| |- app-routing.module.ts app routes
|- assets/ app assets (images, fonts, sounds...)
|- environments/ values for various build environments
|- theme/ app global scss variables and theme
|- translations/ translations files
|- index.html html entry point
|- styles.scss global style entry point
|- main.ts app entry point
|- polyfills.ts polyfills needed by Angular
+- test.ts unit tests entry point
```

# Main tasks

Task automation is based on [NPM scripts](https://docs.npmjs.com/misc/scripts).

| Tasks                        | Description                                                                     |
| ---------------------------- | ------------------------------------------------------------------------------- |
| npm start                    | Run development server on `http://localhost:4200/`                              |
| npm start:staging            | Run development server for staging on `http://localhost:4200/`                  |
| npm start:testing            | Run development server for testing on `http://localhost:4200/`                  |
| npm run build                | Lint code and build app for production in `dist/` folder                        |
| npm run build:staging        | Lint code and build app for staging in `dist/` folder                           |
| npm run build:testing        | Lint code and build app for testing in `dist/` folder                           |
| npm test                     | Run unit tests via [Karma](https://karma-runner.github.io) in watch mode        |
| npm run test:ci              | Lint code and run unit tests once for continuous integration                    |
| npm run e2e                  | Run e2e tests using [Protractor](http://www.protractortest.org)                 |
| npm run lint                 | Lint code                                                                       |
| npm run translations:extract | Extract strings from code and templates to `src/app/translations/template.json` |
| npm run bundle-report        | Visualize size of webpack output files with an interactive zoomable treemap     |
| npm run docs                 | Display project documentation                                                   |

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change
any of the source files.
You should not use `ng serve` directly, as it does not use the backend proxy configuration by default.

## Code scaffolding

Run `npm run generate -- component <name>` to generate a new component. You can also use
`npm run generate -- directive|pipe|service|class|module`.

If you have installed [angular-cli](https://github.com/angular/angular-cli) globally with `npm install -g @angular/cli`,
you can also use the command `ng generate` directly.

## Additional tools

Tasks are mostly based on the `angular-cli` tool. Use `ng help` to get more help or go check out the
[Angular-CLI README](https://github.com/angular/angular-cli).

# What's in the box

The app template is based on [HTML5](http://whatwg.org/html), [TypeScript](http://www.typescriptlang.org) and
[Sass](http://sass-lang.com). The translation files use the common [JSON](http://www.json.org) format.

#### Tools

Development, build and quality processes are based on [angular-cli](https://github.com/angular/angular-cli) and
[NPM scripts](https://docs.npmjs.com/misc/scripts), which includes:

- Optimized build and bundling process with [Webpack](https://webpack.github.io)
- [Development server](https://webpack.github.io/docs/webpack-dev-server.html) with backend proxy and live reload
- Cross-browser CSS with [autoprefixer](https://github.com/postcss/autoprefixer) and
  [browserslist](https://github.com/ai/browserslist)
- Unit tests using [Jasmine](http://jasmine.github.io) and [Karma](https://karma-runner.github.io)
- End-to-end tests using [Protractor](https://github.com/angular/protractor)
- Static code analysis: [TSLint](https://github.com/palantir/tslint), [Codelyzer](https://github.com/mgechev/codelyzer),
  [Stylelint](http://stylelint.io) and [HTMLHint](http://htmlhint.com/)

#### Libraries

- [Angular](https://angular.io)
- [Flex-Layout](https://github.com/angular/flex-layout)
- [Angular Material](https://github.com/angular/material2)
- [RxJS](http://reactivex.io/rxjs)
- [ngx-translate](https://github.com/ngx-translate/core)

#### Coding guides

- [Angular](docs/coding-guides/angular.md)
- [TypeScript](docs/coding-guides/typescript.md)
- [Sass](docs/coding-guides/sass.md)
- [HTML](docs/coding-guides/html.md)
- [Unit tests](docs/coding-guides/unit-tests.md)
- [End-to-end tests](docs/coding-guides/e2e-tests.md)
